//LIBRERIAS IMPORTADAS
//#include <PS2Mouse.h>


//CONSTANTES
#define sensor A7
#define ledIR1 4
#define ledIR2 5

//VARIABLES
int lecturaMod = 410;

void setup() { //FUNCION QUE SE EJECUTA AL INICIO

  //mouse.initialize();
  Serial.begin(9600,SERIAL_8E1);
  pinMode(ledIR1,OUTPUT);
  pinMode(ledIR2,OUTPUT);
  pinMode(2,INPUT_PULLUP); //BOTON ESPECIAL Borrado
  pinMode(3, INPUT_PULLUP);  //BOTON ESPECIAL color
  attachInterrupt(0,borrar,FALLING); //INTERRUPCIONES PARA LOS BOTONES DE CALIBRACION (0->pin2; 1->pin3; RISING->cuando pasa de low a high)
  attachInterrupt(1,color,FALLING);
}

void loop() {

  if(analogRead(sensor) >= lecturaMod)
  {
    digitalWrite(ledIR1,HIGH);
    digitalWrite(ledIR2,HIGH);
  }else
    {
    digitalWrite(ledIR1,LOW);
    digitalWrite(ledIR2,LOW);
    }  //SI ESTA CERCA SE PRENDE EL LED, SI NO SE APAGA

}

void borrar()
{
  Serial.println("*Borrar*");
}

void color()
{
  Serial.println("*Color*");
}
