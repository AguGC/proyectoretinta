/**
 * @file Permite la administracion de los asistentes de una clase
 * @name assists-model
 * @requires path,assists-connection
 * @author Retinta Team
 * @version 1.0.0 
 */
'use strict'

const path = require('path'), 
      AssistsConn = require(path.join(__dirname,'.','assists-connection'));

/**
 * @class
 * @memberof assists-model
 */       
var AssistsModel = function () {};

/**
 * @function saveAssist() - Guarda un asistente de una clase
 * @param {string} userId - Id de asistente
 * @param {string} classId - Id de clase
 * @param {function} cb - Funcion que se ejecuta si el asistente se almacena correctamente
 * @memberof assists-model.AssistsModel
 * @instance
 */
AssistsModel.saveAssist = function(userId, classId, cb) {
    let assist = new AssistsConn({
        classId: classId,
        userId: userId
    })
    assist.save(function(err) {
        if(err) cb(err)
        cb(null)
    })
}

module.exports = AssistsModel