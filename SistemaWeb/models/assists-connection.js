/**
 * @file Establece el esquema de asistentes almacenado en 
 * base de datos Mongo. Ademas establece parametros de comunicacion con base de datos
 * @name assists-connection
 * @requires mongoose,mongoose.Schema,mongoose.Schema.Types.ObjectId
 * @author Retinta Team
 * @version 1.0.0 
 */
'use strict'

const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      ObjectId = mongoose.Schema.Types.ObjectId,
      AssistsSchema = new Schema({
          classId: {type: ObjectId},
          userId: {type: ObjectId}
      },
{
    collection: 'assists'
})

var AssistsModel = mongoose.model('Assist', AssistsSchema)
module.exports = AssistsModel