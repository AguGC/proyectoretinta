/**
 * @file Valida si los datos ingresados cumplen con ciertos estandares definidos
 * @name dataValidation
 * @author Retinta Team
 * @version 1.0.0 
 */
'use strict'

/**
 * @class
 * @memberof dataValidation
 */
var Checker = () => {};

/**
 * @function onlyNumbers() - Valida si los datos son solo numeros
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.onlyNumbers = (data) => {
    let numbers = /^[0-9]*$/;
    return numbers.test(data);
};

/**
 * @function onlyLetters() - Valida si los datos son solo letras
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.onlyLetters = (data) => {
    let lettersPattern = /^[a-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœA-Z\s]*$/;
    return lettersPattern.test(data);
};

/**
 * @function onlyNumbersAndLetters() - Valida si los datos son solo numeros y letras
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.onlyNumberAndLetters = (data) => {
    let numAndLetters = /^[a-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœA-Z\s0-9]*$/;
    return numAndLetters.test(data);
};

/**
 * @function isMail() - Valida si los datos corresponden a un formato de mail
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.isMail = (data) => {
    let email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return email.test(data);
};

/**
 * @function between() - Valida si el largo de los datos se encuentra entre 
 * dos cotas
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.between = (data, min, max) => {
    return (data.length >= min && data.length <= max); 
};

/**
 * @function isNotEmpty() - Valida si los datos no son vacios
 * @param {Object} data - Datos a validar 
 * @memberof dataValidation.Checker
 * @instance
 */
Checker.isNotEmpty = (data) =>{
    return (data.length > 0)
};
module.exports = Checker;