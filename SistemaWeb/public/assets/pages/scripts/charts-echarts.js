jQuery(document).ready(function ()
{
    function cargarGraficos (dataClases)
    {
        var vistas = $('#vistas');
        vistas.empty();
        vistas.append('<h4>Cantidad de vistas totales: ' + dataClases.totalVistas + '</h4>')
        var mensaje = $('#mensaje');
        // ECHARTS
        require.config({
            paths: {
                echarts: '/assets/global/plugins/echarts/'
            }
        });

        // DEMOS
        require(
            [
                'echarts',
                'echarts/chart/bar',
                //'echarts/chart/chord',
                //'echarts/chart/eventRiver',
                //'echarts/chart/force',
                //'echarts/chart/funnel',
                //'echarts/chart/gauge',
                //'echarts/chart/heatmap',
                //'echarts/chart/k',
                //'echarts/chart/line',
                //'echarts/chart/map',
                'echarts/chart/pie'//,
                //'echarts/chart/radar',
                //'echarts/chart/scatter',
                //'echarts/chart/tree',
                //'echarts/chart/treemap',
                //'echarts/chart/venn',
                //'echarts/chart/wordCloud'
            ],
            function (ec)
            {
                var E = parseInt($('#span-E').text());
                var MB = parseInt($('#span-MB').text());
                var B = parseInt($('#span-B').text());
                var R = parseInt($('#span-R').text());
                var M = parseInt($('#span-M').text());


                mensaje.show();
                // -- BAR --
                var chart1Parent = $('#echarts_bar').parent();
                var chart2Parent = $('#echarts_pie').parent();
                if ((E + MB + B + R + M) > 0)
                {
                    mensaje.hide();
                    chart1Parent.show();
                    chart2Parent.show();
                    var myChart = ec.init(document.getElementById('echarts_bar'));
                    myChart.setOption({
                        title: {
                            text: 'Valoración de la clase'
                        },
                        tooltip: {
                            trigger: 'item',
                            formatter: "{b} : {c}"
                        },
                        toolbox: {
                            show: true,
                            feature: {
                                mark: { show: true },
                                restore: { show: true },
                                saveAsImage: { show: true }
                            }
                        },
                        calculable: true,
                        xAxis: [ {
                            type: 'value',
                            boundaryGap: [ 0, 0.01 ]
                        } ],
                        yAxis: [ {
                            type: 'category',
                            data: [ 'Mala', 'Regular', 'Bueno', 'Muy Bueno', 'Excelente' ]
                        } ],
                        series: [ {
                            name: 'Serie',
                            type: 'bar',
                            data: [ M, R, B, MB, E ]
                        } ]
                    });
                    // -- PIE --
                    var myChart1 = ec.init(document.getElementById('echarts_pie'));
                    myChart1.setOption({
                        title: {
                            text: 'Valoración de la clase',
                            x: 'center'
                        },
                        tooltip: {
                            trigger: 'item',
                            formatter: "{b} : {c}"
                        },
                        legend: {
                            orient: 'vertical',
                            x: 'left',
                            y: 'bottom',
                            data: [ 'Excelente', 'Muy Buena', 'Buena', 'Regular', 'Mala' ]
                        },
                        toolbox: {
                            show: true,
                            feature: {
                                restore: { show: true },
                                saveAsImage: { show: true }
                            }
                        },
                        calculable: true,
                        series: [ {
                            name: 'Valor',
                            type: 'pie',
                            radius: [ '50%', '70%' ],
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: false
                                    },
                                    labelLine: {
                                        show: false
                                    }
                                },
                                emphasis: {
                                    label: {
                                        show: true,
                                        position: 'center',
                                        formatter: "{b} \n {d}%",
                                        textStyle: {
                                            color: 'grey',
                                            fontSize: '30',
                                            fontFamily: '微软雅黑',
                                            fontWeight: 'bold'
                                        }
                                    }
                                }
                            },
                            data: [
                                { value: M, name: 'Mala' },
                                { value: R, name: 'Regular' },
                                { value: B, name: 'Buena' },
                                { value: MB, name: 'Muy Buena' },
                                { value: E, name: 'Excelente' }
                            ]
                        } ]
                    });

                } else
                {
                    chart1Parent.hide();
                    chart2Parent.hide();
                }


                var chart4parent = $('#echarts_pieVotantes').parent();
                //gráfico porcentaje de votantes en las clases
                if (dataClases.porcentajeVotantes)
                {
                    mensaje.hide();
                    chart4parent.show();
                    var myChart6 = ec.init(document.getElementById('echarts_pieVotantes'));
                    myChart6.setOption({
                        title: {
                            text: 'Porcentaje de Votantes',
                            x: 'center'
                        },
                        tooltip: {
                            trigger: 'item',
                            formatter: "{b} : {c}%"
                        },
                        legend: {
                            orient: 'vertical',
                            x: 'left',
                            y: 'bottom',
                            data: [ 'Votaron', 'No Votaron' ]
                        },
                        toolbox: {
                            show: true,
                            feature: {
                                restore: { show: true },
                                saveAsImage: { show: true }
                            }
                        },
                        calculable: true,
                        series: [ {
                            name: 'Valor',
                            type: 'pie',
                            radius: [ '50%', '70%' ],
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: false
                                    },
                                    labelLine: {
                                        show: false
                                    }
                                },
                                emphasis: {
                                    label: {
                                        show: true,
                                        position: 'center',
                                        formatter: "{b} \n {d}%",
                                        textStyle: {
                                            color: 'grey',
                                            fontSize: '30',
                                            fontFamily: '微软雅黑',
                                            fontWeight: 'bold'
                                        }
                                    }
                                }
                            },
                            data: [
                                { value: dataClases.porcentajeVotantes, name: 'Votaron' },
                                { value: (100 - dataClases.porcentajeVotantes).toFixed(2), name: 'No Votaron' },
                            ]
                        } ]
                    });
                } else
                {
                    chart4parent.hide();
                }

                var chart5parent = $('#echarts_piePermanencia').parent();
                //gráfico porcentaje de permanencia en las clases (Porcentaje de Alumnos que se quedaron hasta el final de la clase)
                if (dataClases.porcentajePermanencia)
                {
                    mensaje.hide();
                    chart5parent.show();
                    var myChart7 = ec.init(document.getElementById('echarts_piePermanencia'));
                    myChart7.setOption({
                        title: {
                            text: 'Porcentaje de cumplimiento de asistencia mínima',
                            x: 'center'
                        },
                        tooltip: {
                            trigger: 'item',
                            formatter: "{b} : {c}%"
                        },
                        legend: {
                            orient: 'vertical',
                            x: 'left',
                            y: 'bottom',
                            data: [ 'Cumplieron asistencia mínima', 'No cumplieron asistencia mínima' ]
                        },
                        toolbox: {
                            show: true,
                            feature: {
                                restore: { show: true },
                                saveAsImage: { show: true }
                            }
                        },
                        calculable: true,
                        series: [ {
                            name: 'Valor',
                            type: 'pie',
                            radius: [ '50%', '70%' ],
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: false
                                    },
                                    labelLine: {
                                        show: false
                                    }
                                },
                                emphasis: {
                                    label: {
                                        show: true,
                                        position: 'center',
                                        formatter: "{b} \n {d}%",
                                        textStyle: {
                                            color: 'grey',
                                            fontSize: '30',
                                            fontFamily: '微软雅黑',
                                            fontWeight: 'bold'
                                        }
                                    }
                                }
                            },
                            data: [
                                { value: dataClases.porcentajePermanencia, name: 'Cumplieron asistencia mínima' },
                                { value: (100 - dataClases.porcentajePermanencia).toFixed(2), name: 'No cumplieron asistencia mínima' },
                            ]
                        } ]
                    });
                } else
                {
                    chart5parent.hide();
                }
            }
        );
    }

    function myAjax (cb1)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {

                // obtengo los datos de las clases para crear los graficos
                misClases = JSON.parse(this.responseText)
                cb1(misClases)
            }
        };
        var myURL = window.location.href.split('/');
        var filter = '?idClasses=' + myURL[ myURL.length - 1 ];
        xhttp.open('GET', '/myreports/' + filter, true);
        xhttp.send();
    }
    myAjax(cargarGraficos);

});