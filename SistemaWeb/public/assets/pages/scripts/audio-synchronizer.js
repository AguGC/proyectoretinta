let startTime = Date.now(),
    chronometer,
    stopDrawing = false,
    context,
    screens = [],
    currentScreenIndex = currentDrawingIndex = currentSegmentIndex = 0,
    AudioSynchronizer = () =>
    {
    };

AudioSynchronizer.setScreens = function (storedScreens)
{
    if (storedScreens && Array.isArray(storedScreens))
    {
        screens = storedScreens;
        console.log(`[METHOD:setScreens] [STATUS:SUCCESS] [storedScreens: ${ storedScreens }]`);
    } else
    {
        console.log(`[METHOD:setScreens] [STATUS:FAIL] [storedScreens: ${ storedScreens }]`);
    }
};

AudioSynchronizer.setChronometer = function (chrom)
{
    if (chrom)
    {
        console.log(`[METHOD:setChronometer] [STATUS:SUCCESS] [chronometer: ${ chrom }]`);
        chronometer = chrom
    } else
    {
        console.log(`[METHOD:setChronometer] [STATUS:FAIL] [chronometer: ${ chrom }]`);

    }
};

AudioSynchronizer.setContext = function (canvaContext)
{
    context = canvaContext;
    console.log(`[METHOD:setContext] [STATUS:SUCCESS] [context: ${ context }]`);
};

AudioSynchronizer.stopDrawing = function ()
{
    stopDrawing = true
};

AudioSynchronizer.draw = async function ()
{
    stopDrawing = false;
    if (screens.length > 0)
    {
        if (!context)
        {
            console.log(`[METHOD:draw] [STATUS:FAILURE] [Message: Error! Null context]`);
        }
        console.log(`[METHOD:draw] [STATUS:INITIALIZED]`);
        for (currentScreenIndex; currentScreenIndex < screens.length; currentScreenIndex++)
        {
            for (currentDrawingIndex; currentDrawingIndex < screens[ currentScreenIndex ].length; currentDrawingIndex++)
            {
                console.log(`[METHOD:draw] [drawingIndex:${ currentDrawingIndex }] [CurrentScreen:${ currentScreenIndex }]  [CurrentDrawing:${ currentDrawingIndex }]  [CurrentSegment:${ currentSegmentIndex }]`);
                if (Array.isArray(screens[ currentScreenIndex ][ currentDrawingIndex ].data))
                {
                    let segments = screens[ currentScreenIndex ][ currentDrawingIndex ].data;
                    for (currentSegmentIndex; currentSegmentIndex < segments.length; currentSegmentIndex++)
                    {
                        if ((segments[ currentSegmentIndex ].startTime) > currentTime())
                        {
                            await wait((segments[ currentSegmentIndex ].startTime) - currentTime());
                        }
                        if (currentScreenIndex === 0)
                        {
                            //cleanCanvas(); Causa que se borre todo el canvas al dibujar un trazo
                        }
                        if (stopDrawing)
                        {
                            console.log(`[METHOD:draw] [Status:STOPED] [CurrentScreen:${ currentScreenIndex }]  [CurrentDrawing:${ currentDrawingIndex }]  [CurrentSegment:${ currentSegmentIndex }]`);
                            return;
                        }
                        await simulateDraw(segments[ currentSegmentIndex ]);
                    }
                    currentSegmentIndex = 0;
                } else
                {
                    let letter = screens[ currentScreenIndex ][ currentDrawingIndex ].data;
                    if (letter.startTime > currentTime())
                    {
                        await wait(letter.startTime - currentTime());
                    }
                    if (currentScreenIndex === 0)
                    {
                        //cleanCanvas(); Se borra todo el canvas al escribir
                    }
                    if (stopDrawing)
                    {
                        console.log(`[METHOD:draw] [Status:STOPED] [CurrentScreen:${ currentScreenIndex }]  [CurrentDrawing:${ currentDrawingIndex }]  [CurrentSegment:${ currentSegmentIndex }]`);
                        return;
                    }
                    drawLetter(letter.chart, letter.size, letter.fromX, letter.fromY, letter.color, letter.operation, letter.canvasX, letter.canvasY)
                }
            }
            currentDrawingIndex = 0;
        }
        currentScreenIndex = 0;
        console.log(`[METHOD:draw] [STATUS:FINISHED]`);
    }
};

function previousInstantDrawings ()
{
    if (screens.length > 0)
    {
        console.log(`[METHOD:instantDraw] [STATUS:INITIALIZED] [ScreenIndex:${ currentScreenIndex }] [DrawingIndex:${ currentDrawingIndex }] [SegmentIndex:${ currentSegmentIndex }]`);
        for (let j = 0; j < currentDrawingIndex; j++)
        {
            if (Array.isArray(screens[ currentScreenIndex ][ j ].data))
            {
                let segments = screens[ currentScreenIndex ][ j ].data;
                for (let k = 0; k < segments.length; k++)
                {
                    drawSegment(segments[ k ].fromX, segments[ k ].fromY, segments[ k ].toX, segments[ k ].toY, segments[ k ].color, segments[ k ].size, segments[ k ].operation, segments[ k ].canvasX, segments[ k ].canvasY);
                }
            } else
            {
                let letter = screens[ currentScreenIndex ][ j ].data;
                drawLetter(letter.chart, letter.size ,letter.fromX, letter.fromY, letter.color, letter.operation, letter.canvasX, letter.canvasY)
            }
        }

        if (currentSegmentIndex > 0)
        {
            let segments = screens[ currentScreenIndex ][ currentDrawingIndex ].data;
            for (let k = 0; k < currentSegmentIndex; k++)
            {
                drawSegment(segments[ k ].fromX, segments[ k ].fromY, segments[ k ].toX, segments[ k ].toY, segments[ k ].color, segments[ k ].size, segments[ k ].operation, segments[ k ].canvasX, segments[ k ].canvasY);
            }
        }

        console.log(`[METHOD:instantDraw] [STATUS:FINISHED]`);
    }
}

function currentTime ()
{
    return chronometer.currentTime * 1000
}

function sync ()
{
    currentScreenIndex = currentDrawingIndex = currentSegmentIndex = 0;
    if (screens.length > 0)
    {
        for (let index = 1; index < screens.length; index++)
        {
            if (currentTime() < getDrawingStartTime(screens[ index ][ 0 ]))
            {
                currentScreenIndex = index - 1;
                break;
            }
        }
        if (currentTime() >= getDrawingStartTime(screens[ screens.length - 1 ][ 0 ]))
        {
            currentScreenIndex = screens.length - 1;
        }


        for (let index = 1; index < screens[ currentScreenIndex ].length; index++)
        {
            if (currentTime() < getDrawingStartTime(screens[ currentScreenIndex ][ index ]))
            {
                currentDrawingIndex = index - 1;
                break;
            }
        }
        if (currentTime() >= getDrawingStartTime(screens[ currentScreenIndex ][ screens[ currentScreenIndex ].length - 1 ]))
        {
            currentDrawingIndex = screens[ currentScreenIndex ].length - 1;
        }

        if (Array.isArray(screens[ currentScreenIndex ][ currentDrawingIndex ].data))
        {
            for (let index = 1; index < screens[ currentScreenIndex ][ currentDrawingIndex ].data.length; index++)
            {
                if (currentTime() < screens[ currentScreenIndex ][ currentDrawingIndex ].data[ index ].startTime)
                {
                    currentSegmentIndex = index - 1;
                    break;
                }
            }
            let lastSegmentIndex = screens[ currentScreenIndex ][ currentDrawingIndex ].data.length - 1;
            if (currentTime() >= screens[ currentScreenIndex ][ currentDrawingIndex ].data[ lastSegmentIndex ].startTime)
            {
                currentSegmentIndex = lastSegmentIndex;
            }
        }
        console.log(`[Method:sync] [Status:FINISHED] [currentTime:${ currentTime() }] [CurrentScreen:${ currentScreenIndex }] [CurrentDrawing:${ currentDrawingIndex }] [CurrentSegment:${ currentSegmentIndex }] `);
    }
}

function getDrawingStartTime (drawing)
{
    let drawingStartTime;
    if (Array.isArray(drawing.data))
    {
        drawingStartTime = drawing.data[ 0 ].startTime;
    } else
    {
        drawingStartTime = drawing.data.startTime;
    }
    return drawingStartTime;
}

function simulateDraw (segment)
{
    // console.log(`[METHOD:simulateDraw] [STATUS:INITIALIZED] [segment: ${segment}]`);
    return new Promise((resolve) =>
    {
        drawSegment(segment.fromX, segment.fromY, segment.toX, segment.toY, segment.color, segment.size, segment.operation, segment.canvasX, segment.canvasY);
        setTimeout(function ()
        {
            resolve("done!");
        }, (segment.endOfLineTime - segment.lineStartTime))
    });
}

function wait (time)
{
    return new Promise((resolve) =>
    {
        setTimeout(function ()
        {
            resolve("done!");
        }, (time))
    });
}

function drawSegment (fromX, fromY, toX, toY, color, size, operation, canvasX, canvasY)
{
    let prop = proportion(canvasX, canvasY);

    context.globalCompositeOperation = operation;
    context.strokeStyle = color;
    size *= prop.proportion;
    context.lineWidth = size;
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.beginPath();

    fromX = (fromX * prop.canvasX + prop.Xoffset);
    fromY = (fromY * prop.canvasY + prop.Yoffset);
    toX = (toX * prop.canvasX + prop.Xoffset);
    toY = (toY * prop.canvasY + prop.Yoffset);

    context.moveTo(fromX, fromY);
    context.lineTo(toX, toY);
    context.stroke();
};


function drawLetter (letter,letterSize, x, y, color, operation, canvasX, canvasY)
{
    let prop = proportion(canvasX, canvasY),
        font = letterSize * prop.proportion;

    context.fillStyle = color;
    context.globalCompositeOperation = operation;
    context.font = font.toString() + "pt Arial";
    context.textBaseline = "bottom";
    context.textAlign = "left";
    context.beginPath();

    x = (x * prop.canvasX + prop.Xoffset);
    y = (y * prop.canvasY + prop.Yoffset);

    context.fillText(letter, x, y);
    context.stroke();
};

function proportion (canvasOriginX, canvasOriginY)
{
    let prop = ((canvas.width / canvasOriginX) < (canvas.height / canvasOriginY)) ? (canvas.width / canvasOriginX) : (canvas.height / canvasOriginY),
        canvasX = canvasOriginX * prop,
        canvasY = canvasOriginY * prop,
        Xoffset = Math.abs(canvas.width - canvasX) / 2,
        Yoffset = (canvas.height - canvasY) / 2;

    return {
        canvasX: canvasX,
        canvasY: canvasY,
        Xoffset: Xoffset,
        Yoffset: Yoffset,
        proportion: prop
    }
}

function cleanCanvas ()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
}

AudioSynchronizer.continueDrawing = function ()
{
    console.log(`[Method:continueDrawing] [currentTime:${ currentTime() }]`);
    if (screens.length > 0)
    {
        sync();
        cleanCanvas();
        previousInstantDrawings();
    }
};


$(document).ready(function ()
{
    let canvas = document.getElementById('canvas'),
        context = canvas.getContext('2d'),
        classId = document.getElementById('class-id').value;
    AudioSynchronizer.setContext(context);

    $.ajax({
        url: `${ window.location.origin }/classes/${ classId }/screens`,
        dataType: 'json',
    })
        .done(function (json)
        {
            console.log(`[Method:Ready] [Status:Success] [Json:${ json }]`);
            AudioSynchronizer.setScreens(json);
            audio.readyState = 4;
        })
        .fail(function (jqxhr, textStatus, error)
        {
            console.log(`[Method:Ready] [Status:${ textStatus }] [Error:${ error }]`);
        });
});