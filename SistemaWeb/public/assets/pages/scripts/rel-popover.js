$(function() {
    $('[rel=popover]').popover({
        html: true,
        content: function() {
            return $('#popover_content_wrapper').html();
        },
    });
    $('[rel=popover]').popover().on("show.bs.popover", function() { $(this).data("bs.popover").tip().css("max-width", "600px"); });
});

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
});