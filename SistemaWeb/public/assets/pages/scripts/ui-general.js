var UIGeneral = function() {

    var handlePulsate = function() {
        if (!jQuery().pulsate) {
            return;
        }

        if (App.isIE8() == true) {
            return; // pulsate plugin does not support IE8 and below
        }

        if (jQuery().pulsate) {

            jQuery('#pulsate-once').click(function() {
                $('#pulsate-once-target').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once2').click(function() {
                $('#pulsate-once-target2').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once3').click(function() {
                $('#pulsate-once-target3').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once4').click(function() {
                $('#pulsate-once-target4').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once5').click(function() {
                $('#pulsate-once-target5').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once6').click(function() {
                $('#pulsate-once-target6').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once7').click(function() {
                $('#pulsate-once-target7').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-once8').click(function() {
                $('#pulsate-once-target8').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });


        }
    }

    // var handleDynamicPagination = function() {
    //     $('#dynamic_pager_demo1').bootpag({
    //         paginationClass: 'pagination',
    //         next: '<i class="fa fa-angle-right"></i>',
    //         prev: '<i class="fa fa-angle-left"></i>',
    //         total: 6,
    //         page: 1,
    //     }).on("page", function(event, num) {
    //         $("#dynamic_pager_content1").html("Page " + num + " content here"); // or some ajax content loading...
    //     });

    //     $('#dynamic_pager_demo2').bootpag({
    //         paginationClass: 'pagination pagination-sm',
    //         next: '<i class="fa fa-angle-right"></i>',
    //         prev: '<i class="fa fa-angle-left"></i>',
    //         total: 24,
    //         page: 1,
    //         maxVisible: 6
    //     }).on('page', function(event, num) {
    //         $("#dynamic_pager_content2").html("Page " + num + " content here"); // or some ajax content loading...
    //     });
    // }

    return {
        //main function to initiate the module
        init: function() {
            handlePulsate();
            // handleDynamicPagination();
        }

    };

}();

jQuery(document).ready(function() {
    UIGeneral.init();
});