document.addEventListener('DOMContentLoaded', function ()
{

    var questionnaire = JSON.parse(document.getElementById('span-questionnaire').innerText);
    require.config({
        paths: {
            echarts: '/assets/global/plugins/echarts/'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/bar'
        ],
        function (ec)
        {

            var quantity = corrects = incorrects = respond = norespond = 0;

            var quantityQuestionnaires = 0;
            questionnaire.forEach((element, index) =>
            {
                var size = element.respond.norespond +
                    element.respond.corrects + element.respond.incorrects;

                if (size != 0)
                {
                    norespond += element.respond.norespond / size * 100;
                    quantity += element.respond.corrects / size * 100;

                    quantityQuestionnaires++;


                    var customChart = document.getElementById('customQuestionnaire' + (index + 1));
                    var questionSize = Object.keys(element).sort().filter(function (name) { return /^option/.test(name); }).length;
                    var myDataNames = [];
                    var myDataValues = [];

                    myDataNames.push('Sin Responder');
                    myDataValues.push((element.respond.norespond / size * 100).toFixed(2));
                    myDataNames.push('Correctos');
                    myDataValues.push((element.respond.corrects / size * 100).toFixed(2));
                    myDataNames.push('Incorrectos');
                    myDataValues.push((element.respond.incorrects / size * 100).toFixed(2));

                    for (var i = 0; i < questionSize; i++)
                    {
                        if (element[ 'option' + (i + 1) ].isCorrect)
                        {
                            myDataNames.push('Opción ' + (i + 1) + ': ' + element[ 'option' + (i + 1) ].value + ' (Correcta)');
                        }
                        else
                        {
                            myDataNames.push('Opción ' + (i + 1) + ': ' + element[ 'option' + (i + 1) ].value + ' (Incorrecta)');
                        }
                        myDataValues.push((element.respond[ 'option' + (i + 1) ] / size * 100).toFixed(2));
                    }


                    buildCustomChart(customChart, element.question, myDataNames, myDataValues);
                }
            })

            if (quantityQuestionnaires != 0)
            {

                corrects = parseFloat(quantity / quantityQuestionnaires).toFixed(2);
                norespond = parseFloat(norespond / quantityQuestionnaires).toFixed(2);
                incorrects = 100 - corrects;
                respond = parseFloat(100 - norespond).toFixed(2);
            }

            var chart1Parent = $('#pieQuestionnaireGlobalAccuracy').parent();
            var mensaje = $('#mensaje');


            if ((parseFloat(corrects) + parseFloat(incorrects)) > 0)
            {

                mensaje.hide();
                chart1Parent.show();
                var myChart = ec.init(document.getElementById('pieQuestionnaireGlobalAccuracy'));
                setOptionsPie(myChart, 'Respuestas a Cuestionarios (Promedio)', [ 'Correctas', 'Incorrectas' ], [ [ 'Respuestas', corrects, incorrects ] ], '{b} : {c}%');
            } else
            {
                chart1Parent.hide();
            }

            var chart2Parent = $('#pieQuestionnaireGlobalRespond').parent();
            if ((parseFloat(respond) + parseFloat(norespond)) > 0)
            {
                mensaje.hide();
                chart2Parent.show();
                var myChart1 = ec.init(document.getElementById('pieQuestionnaireGlobalRespond'));
                setOptionsPie(myChart1, 'Participación en Cuestionarios (Promedio)', [ 'Respondió', 'No Respondió' ], [ [ 'Participación', respond, norespond ] ], '{b} : {c}%');
            } else
            {
                chart2Parent.hide();
            }

        }
    );

    function buildCustomChart (domElement, title, options, optionValues)
    {

        buildCustomChartTitle(domElement, title);


        for (var i = 0; i < options.length; i++)
        {

            var barStyle = ''
            switch (options[ i ])
            {
                case 'Sin Responder':
                    barStyle = 'progress-bar-warning';
                    break;
                case 'Correctos':
                    barStyle = 'progress-bar-info';
                    break;
                case 'Incorrectos':
                    barStyle = 'progress-bar-danger';
                    break;
                default:
                    barStyle = 'progress-bar-success';
            }
            buildOption(domElement, options, optionValues, i, barStyle);
        }


    }
    function buildCustomChartTitle (domElement, title)
    {

        var titleElement = document.createElement('H4');
        titleElement.classList.add('text-center')
        titleElement.style = 'font-weight:bold';
        var titleText = document.createTextNode('Pregunta: ' + title);
        titleElement.appendChild(titleText);
        domElement.appendChild(titleElement);
    }
    function buildOption (domElement, options, optionValues, i, barStyle)
    {
        var espacio = document.createElement('BR');
        domElement.appendChild(espacio);


        var optionSpan = document.createElement('SPAN');
        optionSpan.classList.add('input-group-addon');
        var spanText = document.createTextNode(options[ i ]);
        optionSpan.appendChild(spanText);

        domElement.appendChild(optionSpan);

        var optionContainer = document.createElement('DIV');
        optionContainer.classList.add('progress');
        optionContainer.style = 'height: 20px';
        var option = document.createElement('DIV');

        optionContainer.appendChild(option);

        option.classList.add('progress-bar');
        option.classList.add('progress-bar-striped');
        option.classList.add(barStyle);
        option.classList.add('active');
        option.setAttribute('role', 'progressbar');

        if (optionValues[ i ] <= 10)
            option.style = 'width: 10%';
        else
            option.style = 'width: ' + optionValues[ i ] + '%';

        option.setAttribute('aria-valuenow', optionValues[ i ]);
        option.setAttribute('aria-valuemin', '0');
        option.setAttribute('aria-valuemax', '0');
        var optionText = document.createTextNode(optionValues[ i ] + '%');

        option.appendChild(optionText);
        domElement.appendChild(optionContainer);
    }
    function setOptionsPie (myChart, title, possibleOptionNames, possibleOptionValues, helpFormat)
    {

        var mySeries = [];
        var i = 0;
        possibleOptionValues.forEach((value) =>
        {
            var myData = [];
            for (var j = 1; j < value.length; j++)
            {
                myData.push({ value: value[ j ], name: possibleOptionNames[ i ] })
                i++;
            }

            mySeries.push(serieElement(value[ 0 ], myData));

        })

        myChart.setOption({
            title: {
                text: title,
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: helpFormat
            },
            legend: {
                orient: 'vertical',
                x: 'right',
                y: 'bottom',
                data: possibleOptionNames
            },
            toolbox: {
                show: true,
                feature: {
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            },
            calculable: true,
            series: mySeries
        })
    }

    function serieElement (serieName, serieData)
    {
        return {
            name: serieName,
            type: 'pie',
            radius: [ '50%', '70%' ],
            itemStyle: {
                normal: {
                    label: {
                        show: false
                    },
                    labelLine: {
                        show: false
                    }
                },
                emphasis: {
                    label: {
                        show: true,
                        position: 'center',
                        formatter: ' {b} \n {d}% ',
                        textStyle: {
                            color: 'grey',
                            fontSize: '30',
                            fontFamily: '微软雅黑',
                            fontWeight: 'bold'
                        }
                    }
                }
            },
            data: serieData
        }
    }
});