$('#sendComment').click(function (event) {
    event.preventDefault();

    let data = {
        idClass: $('#comment-idClass').val(),
        ownerId: $('#comment-ownerId').val(),
        ownerName: $('#comment-ownerName').val(),
        text: $('#comment-text').val()
    }

    $.post('/CommentClassUpload', data, function (comment) {
        $('#commentList').append('<div class="media">\
                                <div class="media-left">\
                                <a href="#">\
                                <img class="media-object" onerror="this.src=\'/avatarsProfile/avatarUnknownSquare.jpg\'" src="/avatarsProfile/'+ comment.ownerId + '">\
                                </a>\
                                </div>\
                                <div class="media-body">\
                                <h4 class="media-heading">\
                                <a href="/myClasses:'+ comment.ownerMail +'" target="_blank" rel="noopener noreferrer">' + comment.ownerName + '</a>\
                                <span class="c-date">'+ comment.time + '</span>\
                                </h4>\
                                <p>'+ comment.text + '</p>	\
                                </div>\
                                </div>');
        $('#comment-text').val('');
        var cantidadComm = $('#cantComm');
        var cantidad = cantidadComm.text();
        var num = parseInt(cantidad.substr(13, cantidad.length - 14));

        cantidadComm.text('Comentarios (' + (num + 1) + ')');
    });


});   
