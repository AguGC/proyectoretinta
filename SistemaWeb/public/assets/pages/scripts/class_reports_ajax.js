var idOwner
var idClases
var misClases
var primeraVez = true;
function agregarClases ()
{
    idClases = []
    var chkClases = document.getElementsByClassName('chkClase')
    for (let i = 0; i < chkClases.length; i++)
    {
        if (chkClases[ i ].checked)
        {
            idClases.push(chkClases[ i ].value)
        }
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {

            // obtengo los datos de las clases para crear los graficos
            misClases = JSON.parse(this.responseText)
            cargarGraficos(misClases)
        }
    };
    xhttp.open('GET', '/myreports/?idClasses=' + idClases.toString(), true);
    xhttp.send();
}


function cargarGraficos (dataClases)
{

    var vistas = $('#vistas');
    vistas.empty();
    vistas.append('<h4>Cantidad de vistas totales: ' + dataClases.totalVistas + '</h4>')
    //ECHARTS
    require.config({
        paths: {
            echarts: '/assets/global/plugins/echarts/'
        }
    });

    // DEMOS
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/pie'
        ],
        function (ec)
        {
            var E = dataClases.valoracion.cantidadE;
            var MB = dataClases.valoracion.cantidadMB;
            var B = dataClases.valoracion.cantidadB;
            var R = dataClases.valoracion.cantidadR;
            var M = dataClases.valoracion.cantidadM;


            var Correctas = dataClases.cuestionarios.cantidadCorrectas;
            var Incorrectas = dataClases.cuestionarios.cantidadIncorrectas;
            var NoRespondidas = dataClases.cuestionarios.cantidadNoRespondidas

            var mensaje = $('#mensaje');
            mensaje.show();
            var chart1Parent = $('#echarts_bar').parent();
            var chart2Parent = $('#echarts_pie').parent();
            if ((E + MB + B + R + M) > 0)
            {
                mensaje.hide();
                chart1Parent.show();
                chart2Parent.show();
                // -- BAR FOR VALORACION --
                var myChart = ec.init(document.getElementById('echarts_bar'));
                myChart.setOption({
                    title: {
                        text: 'Valoración de la clase'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c}"
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: { show: true },
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    calculable: true,
                    xAxis: [ {
                        type: 'value',
                        boundaryGap: [ 0, 0.01 ]
                    } ],
                    yAxis: [ {
                        type: 'category',
                        data: [ 'Mala', 'Regular', 'Bueno', 'Muy Bueno', 'Excelente' ]
                    } ],
                    series: [ {
                        name: 'Serie',
                        type: 'bar',
                        data: [ M, R, B, MB, E ]
                    } ]
                });
                // -- PIE PARA VALORACION -- 
                var myChart5 = ec.init(document.getElementById('echarts_pie'));
                myChart5.setOption({
                    title: {
                        text: 'Valoración de la clase',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c}"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        y: 'bottom',
                        data: [ 'Excelente', 'Muy Buena', 'Buena', 'Regular', 'Mala' ]
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    calculable: true,
                    series: [ {
                        name: 'Valor',
                        type: 'pie',
                        radius: [ '50%', '70%' ],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    formatter: "{b} \n {d}%",
                                    textStyle: {
                                        color: 'grey',
                                        fontSize: '30',
                                        fontFamily: '微软雅黑',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: [
                            { value: M, name: 'Mala' },
                            { value: R, name: 'Regular' },
                            { value: B, name: 'Buena' },
                            { value: MB, name: 'Muy Buena' },
                            { value: E, name: 'Excelente' }
                        ]
                    } ]
                });
            } else
            {
                chart1Parent.hide();
                chart2Parent.hide();
            }
            //FIN VALORACION

            var chart3parent = $('#echarts_pieResponds').parent();
            //Bar FOR RESPONDS
            if ((Correctas + Incorrectas + NoRespondidas) > 0)
            {
                mensaje.hide();
                chart3parent.show();
                var myChart = ec.init(document.getElementById('echarts_pieResponds'));
                myChart.setOption({
                    title: {
                        text: 'Respuestas a Cuestionarios (Promedio)'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c} Estudiante/s"
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: { show: true },
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    calculable: true,
                    xAxis: [ {
                        type: 'value',
                        boundaryGap: [ 0, 0.01 ]
                    } ],
                    yAxis: [ {
                        type: 'category',
                        data: [ 'Correctas', 'Incorrectas', 'No Respondidas' ]
                    } ],
                    series: [ {
                        name: 'Serie',
                        type: 'bar',
                        data: [ Correctas, Incorrectas, NoRespondidas ]
                    } ]
                });
            } else
            {
                chart3parent.hide();
            }
            //FIN BAR RESPONDS

            var chart4parent = $('#echarts_pieVotantes').parent();
            //gráfico porcentaje de votantes en las clases
            if (dataClases.porcentajeVotantes)
            {
                mensaje.hide();
                chart4parent.show();
                var myChart6 = ec.init(document.getElementById('echarts_pieVotantes'));
                myChart6.setOption({
                    title: {
                        text: 'Porcentaje de Votantes',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c}%"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        y: 'bottom',
                        data: [ 'Votaron', 'No Votaron' ]
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    calculable: true,
                    series: [ {
                        name: 'Valor',
                        type: 'pie',
                        radius: [ '50%', '70%' ],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    formatter: "{b} \n {d}%",
                                    textStyle: {
                                        color: 'grey',
                                        fontSize: '30',
                                        fontFamily: '微软雅黑',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: [
                            { value: dataClases.porcentajeVotantes, name: 'Votaron' },
                            { value: (100 - dataClases.porcentajeVotantes).toFixed(2), name: 'No Votaron' },
                        ]
                    } ]
                });
            } else
            {
                chart4parent.hide();
            }

            var chart5parent = $('#echarts_piePermanencia').parent();
            //gráfico porcentaje de permanencia en las clases (Porcentaje de Alumnos que se quedaron hasta el final de la clase)
            
            if (dataClases.porcentajePermanencia)
            {
                mensaje.hide();
                chart5parent.show();
                var myChart7 = ec.init(document.getElementById('echarts_piePermanencia'));
                myChart7.setOption({
                    title: {
                        text: 'Porcentaje de cumplimiento de asistencia mínima',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c}%"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        y: 'bottom',
                        data: [ 'Cumplieron asistencia mínima', 'No cumplieron asistencia mínima' ]
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    calculable: true,
                    series: [ {
                        name: 'Valor',
                        type: 'pie',
                        radius: [ '50%', '70%' ],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    formatter: "{b} \n {d}%",
                                    textStyle: {
                                        color: 'grey',
                                        fontSize: '30',
                                        fontFamily: '微软雅黑',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: [
                            { value: dataClases.porcentajePermanencia, name: 'Cumplieron asistencia mínima' },
                            { value: (100 - dataClases.porcentajePermanencia).toFixed(2), name: 'No cumplieron asistencia mínima' },
                        ]
                    } ]
                });
            } else
            {
                chart5parent.hide();
            }
        }
    );
}
function cargarModal (dataClases)
{
    var assetList = $('#checkbox')
    $.each(dataClases.asistencia, function (i, clase)
    {
        var div = $('<div/>')
            .addClass('input-group')
            .appendTo(assetList);
        var icheck = $('<div>')
            .addClass('icheck-list')
            .appendTo(div);

        var label = $('<label/>')
            .appendTo(icheck);


        var icheckbox = $('<div>')
            .addClass('icheckbox_minimal-grey')
            .attr('style', 'position: relative;background:none')
            .appendTo(label);

        var textClass = $('<font/>')
            .attr('style', 'vertical-align: inherit;')
            .text('' + clase.claseName)
            .appendTo(label)

        var input = $('<input/>')
            .addClass('chkClase')
            .addClass('icheck')
            .attr('type', 'checkbox')
            .attr('style', 'position: absolute; position: absolute;height: 18px;width: 18px;top: -1;margin-top: 0px;margin-right: 5px;bottom: 1px;')
            .attr('value', clase.id);

        if (primeraVez)
        {
            input.attr('checked', 'true');

        }
        input.appendTo(icheckbox);

    });
    primeraVez = false;
}

//Create Checkbox
function openModal ()
{
    $('#modal-AllClases').modal();
}

function closeModal ()
{
    $('#modal-AllClases').modal('hide');
}

//


jQuery(document).ready(function ()
{
    idOwner = document.getElementById('inputOwnerId').value
    myAjax(cargarGraficos, cargarModal)
    function myAjax (cb1, cb2)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {

                // obtengo los datos de las clases para crear los graficos
                misClases = JSON.parse(this.responseText)
                cb1(misClases)
                cb2(misClases)
            }
        };
        xhttp.open('GET', '/myreports/', true);
        xhttp.send();
    }


});


document.getElementById('btnAgregarClase').addEventListener('click', agregarClases)
document.getElementById('btnAddClass').addEventListener('click', openModal)

