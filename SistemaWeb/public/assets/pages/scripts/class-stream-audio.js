var socket = io();
var role;
var flag = true;
var flag1 = false;
var flag2 = true;
var peer = [];
var owner = document.getElementById('inputOwnerId').value;
var classId = document.getElementById('class-id').value;
var audio = document.querySelector('audio');
var audioState = $('#audioStreamState');
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

socket.emit('role', { owner: owner, classId: classId });

socket.on('role', function (data) {

	if (flag) {
		role = data.role;
		flag = false;
		play();
	}
});

function play() {
	if (role == "server") {
		//Soy server
		var localStream;
		if (navigator.getUserMedia) {
			navigator.getUserMedia({
				audio: true,
				video: false
			}, function (stream) {
				localStream = stream;
				var AudioContext = window.AudioContext || window.webkitAudioContext;
				var audioCtx = new AudioContext();
				var source = audioCtx.createMediaStreamSource(stream);
				var recorder = audioCtx.createScriptProcessor(2048, 1, 1);
				recorder.onaudioprocess = function (e) {
					if (flag1) {

						var left = convertFloat32ToInt16(e.inputBuffer.getChannelData(0));
						socket.emit('save', left);
						if (flag2 == false) {
							audioState.css('color', 'green');
							audioState.text('Transmitiendo Micrófono a Alumnos');
						}
						flag2 = true;
					} else if (flag2) {
						audioState.css('color', 'red');
						audioState.text('Transmisión de Micrófono Pausada (NO HAY ALUMNOS CONECTADOS)')
						flag2 = false;
					}
				};
				source.connect(recorder);
				recorder.connect(audioCtx.destination);

			}, function (err) {
				alert(err);
			});
		} else {
			alert('Tu navegador no soporta getUserMedia; por favor actualícelo :(');
		}
		socket.on('disconnection', function (client) {
			var index = peer.findIndex(function (element) {
				return element[1] == client;
			});

			peer[index][0].close();
			peer.splice(index, 1);
			if (peer.length == 0) {
				flag1 = false;
			}
		});
		socket.on('client', function (data) {
			flag1 = true;
			peer.push([new RTCPeerConnection(null), data.destination]);
			peer[peer.length - 1][0].addStream(localStream);
			peer[peer.length - 1][0].createOffer(function (desc) {
				peer[peer.length - 1][0].setLocalDescription(new RTCSessionDescription(desc));
				socket.emit('message', { destination: peer[peer.length - 1][1], desc: desc });
			},
				function (err) {
					alert(err);
				});
			peer[peer.length - 1][0].onicecandidate = function (event) {
				socket.emit('message', { destination: peer[peer.length - 1][1], candidate: event.candidate });
			}
		});
	} else {
		//Soy client
		audio.setAttribute('controls', '')
		var server;
		socket.on('server', function (data) {
			server = data.destination;
		});
		peer.push(new RTCPeerConnection(null));
		socket.emit('client', {});

		peer[peer.length - 1].onaddstream = function (event) {
			audio.srcObject = event.stream;
			audio.play();
		};
		peer[peer.length - 1].onicecandidate = function (event) {
			socket.emit('message', { destination: server, candidate: event.candidate });
		}
	}


	socket.on('message', function (data) {
		if (data.candidate) {
			if (role == "client") {
				peer[peer.length - 1].addIceCandidate(new RTCIceCandidate(data.candidate));
			} else {
				peer[peer.length - 1][0].addIceCandidate(new RTCIceCandidate(data.candidate));
			}

		} else if (data.desc) {

			if (role == "client") {
				peer[peer.length - 1].setRemoteDescription(new RTCSessionDescription(data.desc));
				peer[peer.length - 1].createAnswer(function (desc) {
					peer[peer.length - 1].setLocalDescription(new RTCSessionDescription(desc));
					socket.emit('message', { destination: server, desc: desc });
				},
					function (err) {
						alert(err)
					});
			} else {
				peer[peer.length - 1][0].setRemoteDescription(new RTCSessionDescription(data.desc));
			}
		}
	});

}

function convertFloat32ToInt16(buffer) {
	l = buffer.length;
	buf = new Int16Array(l);
	while (l--) {
		buf[l] = Math.min(1, buffer[l]) * 0x7FFF;
	}
	return buf.buffer;
}