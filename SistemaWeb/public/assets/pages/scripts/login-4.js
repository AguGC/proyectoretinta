var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                userMail: {
						required: true,
						email:true
	                },
	                userPassword: {
	                    required: true
	                },
	                remember: {
	                    required: false
					}
					
	            },

	            messages: {
	                userMail: {
						required: "Este campo es requerido.",
						email: "Por favor, introduce una dirección de correo electrónico válida.",
	                },
	                userPassword: {
						required: "Este campo es requerido.",
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
				   $('.alert-danger', $('.login-form')).show();
				   var errors = validator.numberOfInvalids();
				   if (errors) {                    
					   validator.errorList[0].element.focus();
				   }
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
						required: "Este campo es requerido.",
						email:"Por favor, introduce una dirección de correo electrónico válida."

	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
					$('.alert-danger', $('.forget-form')).show();
					var errors = validator.numberOfInvalids();
					if (errors) {                    
						validator.errorList[0].element.focus();
					}
					
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function () {

		        function format(state) {
            if (!state.id) { return state.text; }
            var $state = $(
             '<span><img src="../assets/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
            );
            
            return $state;
        }

        if (jQuery().select2 && $('#country_list').size() > 0) {
            $("#country_list").select2({
	            placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
	            templateResult: format,
                templateSelection: format,
                width: 'auto', 
	            escapeMarkup: function(m) {
	                return m;
	            }
	        });


	        $('#country_list').change(function() {
	            $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
	        });
    	}


         $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
						required: true,
						letras:true,
						minlength:3,
						maxlength:53
					},
					lastname:{
						required: true,
						letras:true,
						minlength:3,
						maxlength:53
					},
					address:{
						required: true,
						minlength:3,
						maxlength:53
					},
					city:{
						required:true,
						letras:true,
						minlength:3,
						maxlength:53
					},
					country:{
						required:true,
					},
	                userMail: {
	                    required: true,
						email: true,
						maxlength:53,
						remote:{
							url:'/ajax',
							type:'post',
							data:{
								userEmail: function(){
									return $('#myEmail').val();
								}
							}
						}
	                },
	                username: {
						required: true,
						minlength:4,
						maxlength:53
	                },
	                userPassword: {
						required: true,
						minlength:8,
						maxlength:16
	                },
	                rpassword: {
						required:true,
						equalTo: "#register_password",
						minlength:8,
						maxlength:16
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Debe aceptar las políticas y términos de Servicio."
					},
					userMail:{
						remote:"Este mail está en uso, por favor eliga otro",
					}
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
					var errors = validator.numberOfInvalids();
					if (errors) {                    
						validator.errorList[0].element.focus();
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();    

            
        }
    };

}();

jQuery(document).ready(function() {
	
	Login.init();
	
	
});