(function () {
  if (!document.addEventListener) {
    document.getElementById('message').innerText =
      'Por favor actualice su navegador';
    document.getElementById('toastTypeGroup').value = 'error';
    document.getElementById('showtoast').click();
  }
  navigator.getUserMedia =
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia;
  var flag = true,
    xpositionFirstLetter = null,
    pressMouseButton = false,
    timeFinishClass = 0,
    myText = [],
    connectionStartTime = null,
    yavote = false,
    quieroSalir = true,
    endClass = '0',
    peer = [],
    owner = document.getElementById('inputOwnerId').value,
    classId = document.getElementById('class-id').value,
    audio = document.querySelector('audio'),
    totalStudents = 0,
    studentsWhoAnswered = 0,
    studentsWhoAnswerRight = 0,
    studentsWhoAnswerWrong = 0,
    studentOptions = {};
  (numberOfExtraQuestions = 2),
    (correctAns = []),
    (savedQuestionnaire = {}),
    (audioY = false),
    (yesLetter = false),
    (role = ''),
    (continueDrawing = true),
    (socket = io()),
    (after = false),
    (firstTime = true),
    (rightClick = false),
    (onViewMode = false),
    (iniciando = true),
    (receivePackage = {}),
    (myChat = $('.myChat')[0]),
    (localStream = null),
    (classStartTime = -1),
    (screenProportion = 0.85),
    (events = {}),
    (increment = 1),
    (secondsToTime = function (s) {
      function addZ(n) {
        return (n < 10 ? '0' : '') + n;
      }

      var ms = s % 1000;
      s = (s - ms) / 1000;
      var secs = s % 60;
      s = (s - secs) / 60;
      var mins = s % 60;
      var hrs = (s - mins) / 60;

      return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
    }),
    (fullscreenChange = function () {
      var can = document.getElementById('canvas');
      if (can.style.width.toString() != '100%') {
        can.style.width = '100%';
        can.style.height = '100%';
      } else {
        can.style.height =
          (window.outerHeight * screenProportion).toString() + 'px';
        can.style.width =
          (window.outerWidth * screenProportion).toString() + 'px';
      }
    }),
    (onResizeEvent = function () {
      if (after == false || role == 'client') {
        var lienzo = document.getElementById('canvas'),
          defaultMaster = document.getElementsByClassName(
            'note-editor note-frame panel panel-default'
          )[0];

        lienzo.setAttribute('height', screen.height);
        lienzo.setAttribute('width', screen.width);
        defaultMaster.style.width =
          (window.outerWidth * screenProportion).toString() + 'px';
        defaultMaster.style.float = 'none';
        defaultMaster.style.margin = '0 auto';
        if (lienzo.style.height.toString() != '100%') {
          lienzo.style.height =
            (window.outerHeight * screenProportion).toString() + 'px';
          lienzo.style.width =
            (window.outerWidth * screenProportion).toString() + 'px';
          var audioTag = document.getElementsByTagName('audio')[0];
          if (audioTag)
            audioTag.style.width = screenProportion * window.outerWidth + 'px';
        }
        if (after) {
          lienzo.getContext('2d').clearRect(0, 0, lienzo.width, lienzo.height);
          socket.emit(events.newScreen, {});
        }
        after = true;
      }
      // $('.panel-body .myChat').css('height',window.innerHeight/2+'px');
      // $('#answer-panel').css('height',window.innerHeight/2+'px');
    });
  var ComponentsEditors = (function () {
    var ui = $.summernote.ui;

    var reviewButton = function (context) {
      var button = ui.button({
        className: 'note-reviewButton',
        contents: '<i class="fa fa-eye" aria-hidden="true"></i>',
        // contents: '<i class="fa fa-folder" aria-hidden="true"></i>',
        tooltip: 'Pizarras guardadas',
        click: function () {},
        data: {
          isReviewing: false,
        },
      });
      return button.render();
    };

    var aheadButton = function (context) {
      var button = ui.button({
        className: 'note-aheadButton',
        contents: '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
        tooltip: 'Adelante',
        click: function () {},
      });
      return button.render();
    };
    var behindButton = function (context) {
      var button = ui.button({
        className: 'note-behindButton',
        contents: '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
        tooltip: 'Atrás',
        click: function () {},
      });
      return button.render();
    };

    var saveButton = function (context) {
      var button = ui.button({
        className: 'note-saveButton',
        contents: '<i class="fa fa-lg fa-floppy-o"/>',
        tooltip: 'Guardar Pizarra (ALT+S)',
        click: function () {},
      });
      return button.render();
    };

    var canvasFullscreen = function (context) {
      var button = ui.button({
        className: 'note-fullscreen',
        contents: '<i class="fa fa-arrows-alt" aria-hidden="true"></i>',
        tooltip: 'Fullscreen',
        click: function () {
          if (document.fullscreenEnabled) {
            //ahora al parecer anda antes tenia este comment Ver, esta validacion deberia ser al reves y un !document.fullscreenEnabled jajaj
            var canvas = document.getElementById('canvas');

            if (canvas.webkitRequestFullScreen) {
              canvas.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
              canvas.onwebkitfullscreenchange = fullscreenChange;
            } else if (canvas.mozRequestFullScreen) {
              canvas.mozRequestFullScreen();
              canvas.onmozfullscreenchange = fullscreenChange;
            } else if (canvas.msRequestFullScreen) {
              canvas.msRequestFullScreen();
              canvas.onmsfullscreenchange = fullscreenChange;
            } else if (canvas.requestFullScreen) {
              canvas.requestFullScreen();
              canvas.onfullscreenchange = fullscreenChange;
            }

            //socket.emit('fullscreen',{});
          } else {
            document.getElementById('message').innerText =
              'Por favor actualice su navegador';
            document.getElementById('toastTypeGroup').value = 'error';
            document.getElementById('showtoast').click();
          }
        },
      });
      return button.render();
    };

    var modeButton = function (context) {
      var button = ui.button({
        className: 'note-modeBotton',
        contents: '<i class="fa fa-tumblr" aria-hidden="true"></i>',
        tooltip: 'Modo de Escritura',
      });
      return button.render();
    };

    var undoButton = function (context) {
      var options = context.options;
      var button = ui.button({
        className: 'note-undoButton',
        contents: ui.icon(options.icons.undo),
        tooltip: 'Deshacer (CTRL+Z)',
      });
      return button.render();
    };

    var redoButton = function (context) {
      var options = context.options;
      var button = ui.button({
        className: 'note-redoButton',
        contents: ui.icon(options.icons.redo),
        tooltip: 'Rehacer (CTRL+Y)',
      });
      return button.render();
    };

    var sizeButton = function (context) {
      var options = context.options;
      var lang = options.langInfo;
      return ui
        .buttonGroup({
          className: 'note-pencilSize',
          children: [
            ui.button({
              className: 'note-current-pencilSize-button',
              contents: 'Talle ' + ui.icon(options.icons.caret, 'span'),
              tooltip: 'Tamaño Pincel',
              data: {
                value: '3',
                toggle: 'dropdown',
              },
            }),
            ui.dropdownCheck({
              className: 'dropdown-fontsize',
              checkClassName: options.icons.menuCheck,
              items: [
                '<input type="text" id="pencilSize" name="pencilSize" value="3" />',
              ].join(''),
            }),
          ],
        })
        .render();
    };

    var colorButton = function (context) {
      var options = context.options;
      var lang = options.langInfo;
      return ui
        .buttonGroup({
          className: 'note-color',
          children: [
            ui.button({
              className: 'note-current-color-button-1',
              contents:
                ui.icon(options.icons.pencil + ' note-recent-color-1') + '-1',
              tooltip: 'Color Primario',
              click: function (e) {},
              callback: function ($button) {
                var $recentColor = $button.find('.note-recent-color-1');
                $recentColor.css('background-color', '#000000');
                $button.attr('data-backcolor', '#000000');
              },
            }),
            ui.button({
              className: 'note-current-color-button-2',
              contents:
                ui.icon(options.icons.pencil + ' note-recent-color-2') + '-2',
              tooltip: 'Color Secundario',
              click: function (e) {},
              callback: function ($button) {
                var $recentColor = $button.find('.note-recent-color-2');
                $recentColor.css('background-color', '#FF0000');
                $button.attr('data-backcolor', '#FF0000');
              },
            }),
            ui.button({
              className: 'dropdown-toggle',
              contents: ui.icon(options.icons.caret, 'span'),
              tooltip: 'Cambiar Colores (ALT+X)',
              data: {
                toggle: 'dropdown',
              },
            }),
            ui.dropdown({
              items: [
                '<li>',
                '<div class="btn-group">',
                '  <div class="note-palette-title"> Primario </div>',
                '  <div class="note-holder" data-event="primary"/>',
                '</div>',
                '<div class="btn-group">',
                '  <div class="note-palette-title"> Secundario </div>',
                '  <div class="note-holder" data-event="secondary"/>',
                '</div>',
                '</li>',
              ].join(''),
              callback: function ($dropdown) {
                $dropdown.find('.note-holder').each(function () {
                  var $holder = $(this);
                  $holder.append(
                    ui
                      .palette({
                        colors: options.colors,
                        eventName: $holder.data('event'),
                      })
                      .render()
                  );
                });
              },
              click: function (event) {
                var $button = $(event.target),
                  eventCatched = $button.data('event'),
                  value = $button.data('value');

                if (eventCatched && value) {
                  var $color, $currentButton, eventToTrigger;
                  if (eventCatched === 'primary') {
                    $color = $button
                      .closest('.note-color')
                      .find('.note-recent-color-1');
                    $currentButton = $button
                      .closest('.note-color')
                      .find('.note-current-color-button-1');
                    eventToTrigger = 'primaryColorChange';
                  } else {
                    $color = $button
                      .closest('.note-color')
                      .find('.note-recent-color-2');
                    $currentButton = $button
                      .closest('.note-color')
                      .find('.note-current-color-button-2');
                    eventToTrigger = 'secondaryColorChange';
                  }
                  $color.css('background-color', value);
                  $currentButton.attr('data-backcolor', value);
                  $currentButton.trigger(eventToTrigger);
                }
              },
            }),
          ],
        })
        .render();
    };

    var handleSummernote = function () {
      //PARTE INCOMPATIBLE CON FIREFOX
      $('#summernote_1').summernote({
        toolbar: [
          ['PencilColor', ['colors']],
          ['ModeTool', ['mode']],
          ['PencilSize', ['size']],
          ['View', ['save', 'myUndo', 'myRedo']],
          ['Help', ['myFullscreen']],
          ['Review', ['behind', 'review', 'ahead']],
        ],
        height: 300,

        buttons: {
          save: saveButton,
          colors: colorButton,
          size: sizeButton,
          mode: modeButton,
          myFullscreen: canvasFullscreen,
          myRedo: redoButton,
          myUndo: undoButton,
          behind: behindButton,
          ahead: aheadButton,
          review: reviewButton,
        },
      });
    };

    return {
      //main function to initiate the module
      init: function () {
        handleSummernote();
        $('#pencilSize').ionRangeSlider({
          type: 'single',
          min: 0,
          max: 20,
          step: 1,
          from: 3,
          grid: true,
          from_min: 1,
        });

        var editArea = document.getElementsByClassName('note-editing-area')[0],
          //editPanel = document.getElementsByClassName('note-editable')[0],
          defaultMaster = document.getElementsByClassName(
            'note-editor note-frame panel panel-default'
          )[0],
          lienzo = document.createElement('canvas'),
          dropzone = document.getElementsByClassName('note-dropzone')[0],
          canvasColumn = document.getElementById('canvasColumn'),
          statusBar = document.getElementsByClassName('note-statusbar')[0];

        //defaultMaster.style.margin = '0px';

        editArea.parentElement.removeChild(editArea);
        statusBar.parentElement.removeChild(statusBar);

        lienzo.setAttribute('id', 'canvas');
        lienzo.style.border = '1px solid #8a8a8a';
        lienzo.style.borderTop = 'none';
        lienzo.style.borderRadius = '3px';
        lienzo.style.backgroundColor = 'white';
        canvasColumn.childNodes[0].appendChild(lienzo);
        /*editArea.setAttribute('style', 'overflow: scroll');
            editArea.removeChild(editPanel);
            editArea.appendChild(lienzo);
            */
        dropzone.parentElement.removeChild(dropzone);
        document.getElementsByClassName(
          'note-btn btn btn-default btn-sm note-behindButton'
        )[0].style.visibility = 'hidden';
        document.getElementsByClassName(
          'note-btn btn btn-default btn-sm note-aheadButton'
        )[0].style.visibility = 'hidden';
        onResizeEvent();
      },
    };
  })();

  //--------------------------- Socket IO ------------------------------------------------------------

  function InitWebSocket() {
    if ('WebSocket' in window) {
      //const url = `ws://${document.domain}:3000`,
      (events = {
        connection: 'connection',
        close: 'close',
        newMessage: 'newMessage',
        saveScreen: 'saveScreen',
        newSegment: 'newSegment',
        newLetter: 'newLetter',
        endOfStroke: 'endOfStroke',
        initialLoad: 'initialLoad',
        undo: 'undo',
        redo: 'redo',
        ratingRequest: 'ratingRequest',
        savedScreensRequest: 'savedScreensRequest',
        currentScreensRequest: 'currentScreensRequest',
        role: 'role',
        questionnaire: 'questionnaire',
        numberOfStudents: 'numberOfStudents',
        answer: 'answer',
        deleteWindow: 'deleteWindow',
        salte: 'salte',
        audio: 'audio',
        audioYes: 'audioYes',
        save: 'save',
        message: 'message',
        client: 'client',
        saveQuestionnaire: 'saveQuestionnaire',
        disconnection: 'disconnection',
        server: 'server',
        newScreen: 'newScreen',
        classStartTime: 'classStartTime',
      }),
        (possiblePencilStates = {
          removing: 'destination-out',
          drawing: 'source-over',
          typing: 'insert-text',
        });
      var canvas = document.getElementById('canvas'),
        context = canvas.getContext('2d'),
        pencil = {
          colorPrimary: document.getElementsByClassName(
            'note-current-color-button-1'
          )[0].dataset.backcolor,
          colorSecondary: document.getElementsByClassName(
            'note-current-color-button-2'
          )[0].dataset.backcolor,
          size: document.getElementById('pencilSize').value,
          state: possiblePencilStates.drawing,
        },
        user = {
          name: getCookie('username'),
          id: $('#user-id').text(),
          mail: getCookieMail('usermail'),
          colorChat: 'blue',
          isEnabled: getHabilitation(),
        },
        isOnCanvas = false,
        origin = {},
        startTime,
        actualSavedScreen = 0,
        savedScreens = [];
      //Mensaje que aparece en la apertura de la página

      //-------------------------- Role: Profesor o alumno ------------------------------------------

      if (document.getElementById('startSurvey')) {
        socket.emit(events.role, {
          owner: owner,
          classId: classId,
          server: true,
        });
        $('.noInicio').css('visibility', 'hidden');
        events.saveScreen = 'nada';
        events.newSegment = 'nada';
        events.newLetter = 'nada';
        events.endOfStroke = 'nada';
        events.initialLoad = 'nada';
        events.questionnaire = 'nada';
        events.deleteWindow = 'nada';
        events.salte = 'nada';
        events.audio = 'nada';
        events.audioYes = 'nada';
        events.save = 'nada';
        events.saveQuestionnaire = 'nada';
      } else {
        var a = document.getElementsByClassName('note-PencilColor')[0];
        a.parentElement.removeChild(a);
        a = document.getElementsByClassName('note-ModeTool')[0];
        a.parentElement.removeChild(a);
        a = document.getElementsByClassName('note-PencilSize')[0];
        a.parentElement.removeChild(a);
        a = document.getElementsByClassName('note-View')[0];
        a.parentElement.removeChild(a);
        socket.emit(events.role, {
          owner: owner,
          classId: classId,
          server: false,
        });
      }

      function onmousedown(e) {
        if (role == 'server' && onViewMode == false) {
          if (e.target == canvas) {
            if ('activeElement' in document) document.activeElement.blur();
            pressMouseButton = true;
            yesLetter = true;
            
            e.preventDefault();
            e.stopPropagation();
            if (e.touches) {
              origin.x = e.touches[0].pageX;
              origin.y = e.touches[0].pageY;
            } else {
              origin.x = e.pageX;
              origin.y = e.pageY;
            }
            isOnCanvas = true;
            xpositionFirstLetter = origin.x;
           
            startTime = Date.now() - classStartTime;
            continueDrawing = true;
            if (e.which == 3) rightClick = true;
            else rightClick = false;
          } else {
            yesLetter = false;
            continueDrawing = false;
          }
          myText = [];
        }
      }

      function mousemove(e) {
        var x = e.pageX;
        var y = e.pageY;
        if (
          continueDrawing &&
          pencil.state != possiblePencilStates.typing &&
          role == 'server'
        ) {
          if (user.isEnabled && isOnCanvas) {
            var canvasOffset = offset(canvas);
            var endTime = Date.now() - classStartTime;
            if (e.touches) {
              x = e.touches[0].pageX;
              y = e.touches[0].pageY;
            }

            var segment = {
              fromX: (origin.x - canvasOffset.left) / canvas.offsetWidth,
              fromY: (origin.y - canvasOffset.top) / canvas.offsetHeight,
              toX: (x - canvasOffset.left) / canvas.offsetWidth,
              toY: (y - canvasOffset.top) / canvas.offsetHeight,
              size: pencil.size,
              color: pencil.colorPrimary,
              operation: pencil.state,
              startTime: startTime,
              endTime: endTime,
              canvasX: canvas.width,
              canvasY: canvas.height,
            };

            if (rightClick) {
              //is mouse secondary button pressed

              segment.color = pencil.colorSecondary;
            }

            //socket.emit('prueba', segment);
            socket.emit(events.newSegment, segment);
            drawSegment(
              segment.fromX,
              segment.fromY,
              segment.toX,
              segment.toY,
              segment.color,
              segment.size,
              segment.operation,
              segment.canvasX,
              segment.canvasY
            );

            if (e.touches) {
              origin.x = e.touches[0].pageX;
              origin.y = e.touches[0].pageY;
            } else {
              origin.x = e.pageX;
              origin.y = e.pageY;
            }
            startTime = endTime;
          }
        }
      }
      document.addEventListener('mousedown', onmousedown);
      canvas.ontouchstart = onmousedown;
      canvas.onmousemove = mousemove;
      canvas.ontouchmove = mousemove;
      canvas.ontouchend = function (e) {
        endOfStroke();
      };

      canvas.oncontextmenu = function (e) {
        return false;
      };

      canvas.onmouseup = function (e) {
        endOfStroke();
      };

      canvas.onmouseleave = function (e) {
        endOfStroke();
      };

      //----------------------- Letter on canvas ----------------------------------
      $(window).keypress(function (e) {
        if (role == 'server') {
          if (e.keyCode == 32 && e.target == document.body) e.preventDefault();
          if (yesLetter && e.width != '' ) {
            if (pencil.state == possiblePencilStates.typing) {

              var canvasOffset = offset(canvas);
              if(((origin.y - canvasOffset.top) / canvas.offsetHeight) < 1){
              var endTime = Date.now() - classStartTime;

              if(pressMouseButton){
                origin.y += (pencil.size + 7) * (canvas.offsetHeight / canvas.height);
                pressMouseButton = false;
              }
              var letter = {
                fromX: (origin.x - canvasOffset.left) / canvas.offsetWidth,
                fromY: ((origin.y - canvasOffset.top) / canvas.offsetHeight),
                size: pencil.size + 1,
                color: pencil.colorPrimary,
                chart: String.fromCharCode(e.which),
                startTime: startTime,
                operation: possiblePencilStates.drawing,
                endTime: endTime,
                canvasX: canvas.width,
                canvasY: canvas.height,
              };
              let element ={
                x:origin.x,
                y:origin.y
              } 
              myText.push(element);
              //console.log(JSON.stringify(letter))
              if (rightClick) {
                //is mouse secondary button pressed
                letter.color = pencil.colorSecondary;
              }
              socket.emit(events.newLetter, letter);
              drawLetter(
                false,
                letter.chart,
                letter.size,
                letter.fromX,
                letter.fromY,
                letter.color,
                letter.operation,
                letter.canvasX,
                letter.canvasY
              );
              var textMTX = context.measureText(String.fromCharCode(e.which));
              let myKey = event.keyCode || event.charCode || e.which;
              if(myKey === 13){
                origin.y += (pencil.size + 7) * (canvas.offsetHeight / canvas.height);
                origin.x =  xpositionFirstLetter
              }else{
                    origin.x += textMTX.width * (canvas.offsetWidth / canvas.width);
                  
                    if(((origin.x - canvasOffset.left) / canvas.offsetWidth) >= 0.98){
                      origin.y += (pencil.size + 5) * (canvas.offsetHeight / canvas.height);
                      origin.x =  xpositionFirstLetter
                    }
                
                }
                
                
              
              startTime = endTime;
            }
          }
        }
        }
      });

      //--------------- Aux ---------------------------------------------

      function endOfStroke() {
        if (pencil.state != possiblePencilStates.typing && role == 'server') {
          if (isOnCanvas) {
            isOnCanvas = false;
            socket.emit(events.endOfStroke, 'endOfStroke');
            rightClick = false;
          }
        }
      }

      function offset(element) {
        var rect = element.getBoundingClientRect(),
          scrollLeft =
            window.pageXOffset || document.documentElement.scrollLeft,
          scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return {
          top: rect.top + scrollTop,
          left: rect.left + scrollLeft,
        };
      }

      function getCookie(name) {
        var match = document.cookie.match(
          new RegExp('(^| )' + name + '=([^;]+)')
        );
        if (match) return match[2];
      }

      function getCookieMail(mail) {
        var match = document.cookie.match(
          new RegExp('(^| )' + mail + '=([^;]+)')
        );
        if (match) return decodeURIComponent(match[2]);
      }

      function getHabilitation() {
        if ('true' == document.getElementById('isEnable').value) {
          return true;
        }
        return false;
      }

      function proportion(canvasOriginX, canvasOriginY) {
        var prop =
          canvas.width / canvasOriginX < canvas.height / canvasOriginY
            ? canvas.width / canvasOriginX
            : canvas.height / canvasOriginY;

        var canvasX = canvasOriginX * prop;
        var canvasY = canvasOriginY * prop;

        var Xoffset = Math.abs(canvas.width - canvasX) / 2;
        var Yoffset = (canvas.height - canvasY) / 2;

        return {
          canvasX: canvasX,
          canvasY: canvasY,
          Xoffset: Xoffset,
          Yoffset: Yoffset,
          proportion: prop,
        };
      }
      function drawSegment(
        fromX,
        fromY,
        toX,
        toY,
        color,
        size,
        operation,
        canvasX,
        canvasY
      ) {
        var prop = proportion(canvasX, canvasY);

        context.globalCompositeOperation = operation;
        context.strokeStyle = color;
        size *= prop.proportion;
        context.lineWidth = size;
        context.lineCap = 'round';
        context.lineJoin = 'round';
        context.beginPath();

        fromX = fromX * prop.canvasX + prop.Xoffset;
        fromY = fromY * prop.canvasY + prop.Yoffset;
        toX = toX * prop.canvasX + prop.Xoffset;
        toY = toY * prop.canvasY + prop.Yoffset;

        context.moveTo(fromX, fromY);
        context.lineTo(toX, toY);
        context.stroke();
      }

      function drawLetter(
        flag,
        letter,
        letterSize,
        x,
        y,
        color,
        operation,
        canvasX,
        canvasY
      ) {
        var prop = proportion(canvasX, canvasY);

        var font = letterSize * prop.proportion;
        context.fillStyle = color;
        context.globalCompositeOperation = operation;

        context.font = font.toString() + 'pt Arial';

        context.textBaseline = 'bottom';
        context.textAlign = 'left';
        context.beginPath();
        x = x * prop.canvasX + prop.Xoffset;
        y = y * prop.canvasY + prop.Yoffset;
       
        context.fillText(letter, x, y);
        context.stroke();

        continueDrawing = false;
      }

      function saveCanvas() {
        let classId = document.getElementById('class-id').value;
        socket.emit(events.saveScreen, classId);
        // var image = canvas.toDataURL('image/jpeg'); // IMAGEN DEL LIENZO
        context.clearRect(0, 0, canvas.width, canvas.height);
      }

      function changePencilColorPrimary(event) {
        pencil.colorPrimary = this.dataset.backcolor;
      }

      function changePencilColorSecondary(event) {
        pencil.colorSecondary = this.dataset.backcolor;
      }

      function changePencilSize(event) {
        pencil.size = this.value;
      }

      // ------------------------------ Chat ----------------------------------------
      function sendMessage(event) {
        if (event.keyCode === 13 || this.id == 'messageSendButton') {
          //13 nro de la tecla 'enter'
          var txtMessage = document.getElementById('messageTextBox');
          if (!txtMessage.value) return;

          var time = classStartTime != -1 ? Date.now() - classStartTime : -1;
          var message = {
            user: user.name,
            id: user.id,
            mail: user.mail,
            text: txtMessage.value,
            color: user.colorChat,
            timeMsg: time,
          };
          if (role == 'server') message.server = true;
          socket.emit(events.newMessage, message);
          txtMessage.value = '';
          onNewMessage(message, false);
        }
      }

      //------------------------ Pencil Colors --------------------------------------------
      function changePencilColorPrimary(event) {
        pencil.colorPrimary = this.dataset.backcolor;
      }

      function changePencilColorSecondary(event) {
        pencil.colorSecondary = this.dataset.backcolor;
      }

      function changePencilSize(event) {
        pencil.size = this.value;
      }

      function shortcuts(event) {
        if (event.keyCode == 9 || (event.shiftKey && event.keyCode == 9)) {
          // Apreto tab o shift + tab
          yesLetter = false;
          continueDrawing = false;
        }
        if (
          (event.keyCode == 46 ||
            event.keyCode == 8 ||
            (event.ctrlKey && event.keyCode == 90)) &&
          yesLetter
        ) {
          event.preventDefault();
          event.stopPropagation();
          if(myText.length > 0 || role == 'server'){
            origin.x = myText[myText.length-1].x;
            origin.y = myText[myText.length-1].y;
            myText = myText.slice(0,-1)
          }
          sendUndo();
        }
        if (event.ctrlKey && event.keyCode == 89 && yesLetter) {
          event.preventDefault();
          event.stopPropagation();
          sendRedo();
        }

        if (event.altKey && event.keyCode == 83 && yesLetter) {
          //keyCode of "S"
          event.preventDefault();
          event.stopPropagation();
          saveCanvas();
        } else if (event.altKey && event.keyCode == 88 && yesLetter) {
          //keyCode of "X"
          event.preventDefault();
          event.stopPropagation();
          switchColors();
        }
      }

      function writingMode() {
        switch (pencil.state) {
          case possiblePencilStates.drawing:
            pencil.state = possiblePencilStates.typing;
            this.lastElementChild.className = 'fa fa-pencil';
            this.setAttribute('data-original-title', 'Lápiz');
            break;
          // case possiblePencilStates.removing:
          //     pencil.state = possiblePencilStates.typing;
          //     this.lastElementChild.className = 'fa fa-pencil';
          //     this.setAttribute('data-original-title', 'Lápiz');
          //     break;
          case possiblePencilStates.typing:
            pencil.state = possiblePencilStates.drawing;
            this.lastElementChild.className = 'fa fa-tumblr';
            this.setAttribute('data-original-title', 'Texto');
            break;
        }
      }

      function switchColors() {
        var auxColor,
          primaryButton = document.getElementsByClassName(
            'note-current-color-button-1'
          )[0],
          secondaryButton = document.getElementsByClassName(
            'note-current-color-button-2'
          )[0];
        auxColor = primaryButton.dataset.backcolor;
        primaryButton.dataset.backcolor = secondaryButton.dataset.backcolor;
        secondaryButton.dataset.backcolor = auxColor;
        $icon = $(primaryButton)
          .closest('.note-color')
          .find('.note-recent-color-1');
        $icon.css('background-color', primaryButton.dataset.backcolor);
        $icon = $(secondaryButton)
          .closest('.note-color')
          .find('.note-recent-color-2');
        $icon.css('background-color', secondaryButton.dataset.backcolor);
        auxColor = pencil.colorPrimary;
        pencil.colorPrimary = pencil.colorSecondary;
        pencil.colorSecondary = auxColor;
      }

      // --------------------------- Deshacer / Rehacer-----------------------------------
      function sendUndo() {
        socket.emit(events.undo, 'undo');
      }

      function sendRedo() {
        socket.emit(events.redo, 'redo');
      }

      //--------------------------------------- Class Rating ----------------------------
      // function sendRatingRequest() {
      //     socket.emit(events.ratingRequest, 'Request Ranking');
      // };

      function reviewRequest(event) {
        var isReviewing = this.getAttribute('data-isReviewing');
        if (isReviewing == 'false') {
          this.setAttribute('data-isReviewing', true);
          this.setAttribute('data-original-title', 'Pizarra Actual');
          this.lastElementChild.className = 'fa fa-eye-slash';
          socket.emit(events.savedScreensRequest, 'savedScreensRequest');
          disableToolbar(true);
        } else {
          this.setAttribute('data-isReviewing', false);
          this.setAttribute('data-original-title', 'Pizarras guardadas');
          this.lastElementChild.className = 'fa fa-eye';
          cleanCanvas();
          socket.emit(events.currentScreensRequest, 'currentScreensRequest');
          disableToolbar(false);
        }
        onViewMode = !onViewMode;
      }
      // --------------------------------- Toolbar --------------------------------------------
      function disableToolbar(enable) {
        if (role != 'server') {
          if (enable) {
            document.getElementsByClassName('note-Help')[0].style.visibility =
              'hidden';
            document.getElementsByClassName(
              'note-behindButton'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
              'note-aheadButton'
            )[0].style.visibility = 'visible';
          } else {
            document.getElementsByClassName('note-Help')[0].style.visibility =
              'visible';
            document.getElementsByClassName(
              'note-behindButton'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-aheadButton'
            )[0].style.visibility = 'hidden';
          }
        } else {
          if (enable) {
            document.getElementsByClassName(
              'note-btn-group btn-group note-PencilColor'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-btn-group btn-group note-ModeTool'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-btn-group btn-group note-PencilSize'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-btn-group btn-group note-View'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-btn-group btn-group note-Help'
            )[0].style.visibility = 'hidden';

            document.getElementsByClassName(
              'note-btn btn btn-default btn-sm note-behindButton'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
              'note-btn btn btn-default btn-sm note-aheadButton'
            )[0].style.visibility = 'visible';
          } else {
            document.getElementsByClassName(
              'note-btn-group btn-group note-PencilColor'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
              'note-btn-group btn-group note-ModeTool'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
              'note-btn-group btn-group note-PencilSize'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
              'note-btn-group btn-group note-View'
            )[0].style.visibility = 'visible';
            document.getElementsByClassName(
             'note-btn-group btn-group note-Help'
            )[0].style.visibility = 'visible';

            document.getElementsByClassName(
              'note-btn btn btn-default btn-sm note-behindButton'
            )[0].style.visibility = 'hidden';
            document.getElementsByClassName(
              'note-btn btn btn-default btn-sm note-aheadButton'
            )[0].style.visibility = 'hidden';
          }
        }
      }
      //------------------------ Pantallas guardadas Pre y Post -----------------------------------
      function toBehind() {
        if (savedScreens.length > 0) {
          if (actualSavedScreen > 0) {
            actualSavedScreen--;
          }
          cleanCanvas();
          onNewScreen(savedScreens[actualSavedScreen]);
        }
      }

      function toAhead() {
        if (savedScreens.length > 0) {
          if (actualSavedScreen < savedScreens.length - 1) {
            actualSavedScreen++;
          }
          cleanCanvas();
          onNewScreen(savedScreens[actualSavedScreen]);
        }
      }

      function saveScreen() {
        let classId = document.getElementById('class-id').value;
        socket.emit(events.saveScreen, classId);
        // var image = canvas.toDataURL('image/jpeg'); // IMAGEN DEL LIENZO
        context.clearRect(0, 0, canvas.width, canvas.height);
      }

      //------------------------- Socket EventsListeners Callbacks ---------------------------------------------

      function onRatingRequest(duration) {
        endClass = 1;
        timeFinishClass = Number(duration)
        console.log(timeFinishClass)
        finishClass();
      }

      window.onbeforeunload = function () {
        if (role == 'client' && !yavote) {
          let quitTime = 0;
          if ( classStartTime != -1){
              quitTime = Date.now()-connectionStartTime;

          }else{
            quitTime = 0;
          }
          console.log('datenow: ',Date.now(),', connectionStartTime: ',connectionStartTime)
          vote(
            document.getElementById('class-id').value,
            0,
            document.getElementById('voteOrigin').value,
            quitTime,
            0,
            endClass,
            document.getElementById('voteMethod').value
          );
        }
      };

      function onReview(screens) {
        if (screens != undefined) {
          savedScreens = screens;
          actualSavedScreen = savedScreens.length - 1;
          toAhead();
        }
      }

      function onUndo(drawing) {
        cleanCanvas();
        onNewScreen(drawing);
      }

      // function onUndoStroke(strokeData) {

      //     var operation = possiblePencilStates.removing;
      //     if (Array.isArray(strokeData)) {
      //         if (strokeData[0].operation == possiblePencilStates.removing) {
      //             operation = possiblePencilStates.drawing;
      //         };
      //         for (var i = 0; i < strokeData.length; i++) {
      //             drawSegment(strokeData[i].fromX, strokeData[i].fromY, strokeData[i].toX,
      //                 strokeData[i].toY, strokeData[i].color, strokeData[i].size, operation,strokeData[i].canvasX,strokeData[i].canvasY);
      //         }
      //     } else {
      //         if (strokeData.operation == possiblePencilStates.removing) {
      //             operation = possiblePencilStates.drawing;
      //         };
      //         drawSegment(strokeData.fromX, strokeData.fromY, strokeData.toX,
      //             strokeData.toY, strokeData.color, strokeData.size, operation,strokeData.canvasX,strokeData.canvasY);
      //     }
      // };

      // function onUndoLetter(letterData){
      //     let operationNew = possiblePencilStates.removing;
      //     if (letterData.operation == possiblePencilStates.removing) {
      //         operationNew = possiblePencilStates.drawing;
      //     };
      //     drawLetter(letterData.chart,letterData.fromX, letterData.fromY, letterData.color, operationNew,letterData.canvasX,letterData.canvasY)
      // };

      function onRedo(drawing) {
        if (drawing.type === 'stroke') {
          onRedoStroke(drawing.data);
        } else {
          onRedoLetter(drawing.data);
        }
      }

      function onRedoStroke(strokeData) {
        if (Array.isArray(strokeData)) {
          for (var i = 0; i < strokeData.length; i++) {
            drawSegment(
              strokeData[i].fromX,
              strokeData[i].fromY,
              strokeData[i].toX,
              strokeData[i].toY,
              strokeData[i].color,
              strokeData[i].size,
              strokeData[i].operation,
              strokeData[i].canvasX,
              strokeData[i].canvasY
            );
          }
        } else {
          drawSegment(
            strokeData.fromX,
            strokeData.fromY,
            strokeData.toX,
            strokeData.toY,
            strokeData.color,
            strokeData.size,
            strokeData.operation,
            strokeData.canvasX,
            strokeData.canvasY
          );
        }
      }

      function onRedoLetter(letterData) {
        drawLetter(
          false,
          letterData.chart,
          letterData.size,
          letterData.fromX,
          letterData.fromY,
          letterData.color,
          letterData.operation,
          letterData.canvasX,
          letterData.canvasY
        );
      }

      function cleanCanvas() {
        context.clearRect(0, 0, canvas.width, canvas.height);
      }

      function onNewSegment(segmentData) {
        if (segmentData !== null) {
          drawSegment(
            segmentData.fromX,
            segmentData.fromY,
            segmentData.toX,
            segmentData.toY,
            segmentData.color,
            segmentData.size,
            segmentData.operation,
            segmentData.canvasX,
            segmentData.canvasY
          );
        }
      }

      function onNewLetter(letterData) {
        if (letterData !== null) {
          //console.log('entro x ws lo sig:  ' + JSON.stringify(letter))
          drawLetter(
            true,
            letterData.chart,
            letterData.size,
            letterData.fromX,
            letterData.fromY,
            letterData.color,
            letterData.operation,
            letterData.canvasX,
            letterData.canvasY
          );
        }
      }

      function onNewScreen(drawings) {
        //console.log('drawings XXX:  ' + JSON.stringify(drawings))
        //consultar tipo y entrar aca si es tipo Stroke
        for (i = 0; i < drawings.length; i++) {
          //consultar tipo y entrar aca si es tipo Stroke
          //console.log('type ' + drawings[i].type)
          if (drawings[i].type === 'stroke') {
            if (Array.isArray(drawings[i].data)) {
              // recursive(drawings[i], 0);
              for (j = 0; j < drawings[i].data.length; j++) {
                onNewSegment(drawings[i].data[j]);
              }
            } else {
              onNewSegment(drawings[i].data);
            }
          } else {
            onNewLetter(drawings[i].data);
          }
        }
      }

      function onInitialLoad(data) {
        iniciando = true;
        var drawings = data.drawings;
        var messages = data.chat;

        for (i = 0; i < messages.length; i++) {
          onNewMessage(messages[i], false);
        }

        onNewScreen(drawings);
        iniciando = false;
      }

      function recursive(array, n) {
        onNewSegment(array[n]);
        if (n < array.length) {
          // DATOS SOLO PARA DEBUGGER
          // var time1 = array[n].lineStartTime;
          // var time2 = array[n].endOfLineTime;
          // var time3 = time2 - time1;
          setTimeout(function () {
            recursive(array, ++n);
          }, array[n].endTime - array[n].startTime);
        }
      }
      function fullChatScroll() {
        myChat.scrollTop = myChat.scrollHeight;
      }
      function enableScroll() {
        return (
          parseInt(Math.round(myChat.scrollTop + 0.1 * myChat.scrollHeight)) +
            myChat.offsetHeight >=
          myChat.scrollHeight
        );
      }
      function onNewMessage(message, bandera) {
        var si = enableScroll() || iniciando;

        if (bandera) {
          //Mensaje de otro usuario, no mio
          var messageIcon = document.getElementsByClassName('fa fa-wechat')[0];
          messageIcon.classList.add('text-info');
          setTimeout(function () {
            messageIcon.classList.remove('text-info');
          }, 500);
        }
        //Funcion conversion milisegundos de tiempo en horas minutos y segundos

        var tiempoMilisegundo = `${message.timeMsg}`;

        var tiempoHoras =
          tiempoMilisegundo != -1
            ? secondsToTime(Number(tiempoMilisegundo))
            : 'En espera';
        //fin funcion conversion

        var li = document.createElement('li');
        li.setAttribute('class', 'left clearfix');
        var highlighted;
        //console.log('message --: '+JSON.stringify(message));
        //p.style.color = message.color;
        if (message.server) {
          highlighted = `<p class="text-danger">${message.text}</p>`;
          message.user += ' (DOCENTE)';
        } else highlighted = `<p>${message.text}</p>`;
        var items = [
          `<span class="chat-img pull-left">`,
          `<img class="img-circle-chat" src="/avatarsProfile/${message.id}" onerror="this.src='/avatarsProfile/avatarUnknownSquare.jpg';">`,
          `</span>`,
          `<div class="chat-body clearfix">`,
          `<div class="header">`,
          `<strong class="primary-font">`,
          `<a href ="/myClasses:${message.mail}" target="_blank" rel="noopener noreferrer">${message.user}</a>`,
          `</strong>`,
          `<small class="pull-right text-muted">`,
          `<span class="glyphicon glyphicon-time">`,
          `</span>`,
          tiempoHoras,
          `</small>`,
          `</div>`,
          highlighted,
          `</div>`,
          `</li>`,
        ];
        li.innerHTML = items.join('');
        document.getElementById('chat').appendChild(li);

        if (si) {
          setTimeout(fullChatScroll(), 500);
        }
      }
      //----------------------- End CallBacks-----------------------------------------

      //----------------------- Start Class / Audio ----------------------------------------

      function onRole(data) {
        if (flag) {
          role = data.role;
          uiEvents();
          flag = false;
          if (role == 'client') {
          document.getElementById('beginningMessageAlumno').style.display =
            'block';
          }
          play();
        }
      }

      function startClass() {
        $('#startSurvey').show();
        if (navigator.getUserMedia) {
          navigator.getUserMedia(
            {
              audio: true,
              video: false,
            },
            function (stream) {
              events = {
                connection: 'connection',
                close: 'close',
                newMessage: 'newMessage',
                saveScreen: 'saveScreen',
                newSegment: 'newSegment',
                newLetter: 'newLetter',
                endOfStroke: 'endOfStroke',
                initialLoad: 'initialLoad',
                undo: 'undo',
                redo: 'redo',
                ratingRequest: 'ratingRequest',
                savedScreensRequest: 'savedScreensRequest',
                currentScreensRequest: 'currentScreensRequest',
                role: 'role',
                questionnaire: 'questionnaire',
                numberOfStudents: 'numberOfStudents',
                answer: 'answer',
                deleteWindow: 'deleteWindow',
                salte: 'salte',
                audio: 'audio',
                audioYes: 'audioYes',
                save: 'save',
                message: 'message',
                client: 'client',
                saveQuestionnaire: 'saveQuestionnaire',
                disconnection: 'disconnection',
                server: 'server',
                newScreen: 'newScreen',
                classStartTime: 'classStartTime',
              };
              audioY = true;
              classStartTime = Date.now();
              socket.emit(events.audioYes, {
                owner: owner,
                classStartTime: classStartTime,
              });
              localStream = stream;

              $('.noInicio').css('visibility', 'visible');
              var elem = document.getElementById('beginningMessage').parentNode
                .parentNode.parentNode;

              elem.parentNode.removeChild(elem);
              var AudioContext =
                window.AudioContext || window.webkitAudioContext;
              var audioCtx = new AudioContext();
              var source = audioCtx.createMediaStreamSource(localStream);
              var recorder = audioCtx.createScriptProcessor(2048, 1, 1);

              recorder.onaudioprocess = function (e) {
                var left = convertFloat32ToInt16(
                  e.inputBuffer.getChannelData(0)
                );
                socket.emit(events.save, left);
                if (firstTime) {
                  $('#muteMic').css('display', 'inline');
                  firstTime = false;
                }
              };
              source.connect(recorder);
              recorder.connect(audioCtx.destination);
              socket.on(events.disconnection, function (client) {
                var index = peer.findIndex(function (element) {
                  return element[1] == client;
                });

                peer[index][0].close();
                peer.splice(index, 1);
              });
              socket.on(events.client, function (data) {
                if (audioY) {
                  peer.push([new RTCPeerConnection(null), data.destination]);
                  var indice = peer.length - 1;
                  socket.emit(events.classStartTime, {
                    destination: peer[indice][1],
                    classStartTime: classStartTime,
                  });
                  peer[indice][0].addStream(localStream);
                  peer[indice][0].createOffer(
                    function (desc) {
                      peer[indice][0].setLocalDescription(
                        new RTCSessionDescription(desc)
                      );
                      socket.emit(events.message, {
                        destination: peer[indice][1],
                        desc: desc,
                      });
                    },
                    function (err) {
                      document.getElementById('message').innerText =
                        'Lo sentimos, fallo la conexión';
                      document.getElementById('toastTypeGroup').value = 'error';
                      document.getElementById('showtoast').click();
                    }
                  );
                  peer[indice][0].onicecandidate = function (event) {
                    socket.emit(events.message, {
                      destination: peer[indice][1],
                      candidate: event.candidate,
                    });
                  };
                }
              });
            },
            function (err) {
              document.getElementById('message').innerText =
                'Considere habilitar su micrófono';
              document.getElementById('toastTypeGroup').value = 'info';
              document.getElementById('showtoast').click();
            }
          );
        } else {
          document.getElementById('message').innerText =
            'Por favor actualice su navegador';
          document.getElementById('toastTypeGroup').value = 'error';
          document.getElementById('showtoast').click();
        }

        const chart = document.getElementById('chart');
        if (chart) {
          window.setInterval(function () {
            if (classStartTime != -1) {
              Plotly.extendTraces(
                'chart',
                {
                  x: [
                    [
                      secondsToTime(
                        Number(Date.now() - classStartTime)
                      ).toString(),
                    ],
                  ],
                  y: [[totalStudents]],
                },
                [0]
              );
            }
          }, 60000);
        }
      }

      function play() {
        if (role != 'server') {
          //Soy server
          //var messageB = $('#beginningMessage');
          //messageB.modal('show');
          //message.prop('style','display:inline');

          //Soy client
          audio.setAttribute('controls', '');
          var server;
          socket.on(events.server, function (data) {
            server = data.destination;
          });
          peer.push(new RTCPeerConnection(null));
          socket.emit(events.client, {});

          peer[peer.length - 1].onaddstream = function (event) {
            if (event.stream != null) {
              audio.srcObject = event.stream;
              audio.play();
            }
          };
          peer[peer.length - 1].onicecandidate = function (event) {
            socket.emit(events.message, {
              destination: server,
              candidate: event.candidate,
            });
          };
        }

        socket.on(events.message, function (data) {
          if (data.candidate) {
            if (role == 'client') {
              peer[peer.length - 1].addIceCandidate(
                new RTCIceCandidate(data.candidate)
              );
            } else {
              var indice = peer.findIndex(function (element) {
                return element[1] == data.origin;
              });
              peer[indice][0].addIceCandidate(
                new RTCIceCandidate(data.candidate)
              );
              //termina proceso de inicialización de comunicación para el audio
            }
          } else if (data.desc) {
            if (role == 'client') {
              peer[peer.length - 1].setRemoteDescription(
                new RTCSessionDescription(data.desc)
              );
              peer[peer.length - 1].createAnswer(
                function (desc) {
                  peer[peer.length - 1].setLocalDescription(
                    new RTCSessionDescription(desc)
                  );
                  socket.emit(events.message, {
                    destination: server,
                    desc: desc,
                  });
                },
                function (err) {
                  document.getElementById('message').innerText =
                    'Lo sentimos, fallo la conexión';
                  document.getElementById('toastTypeGroup').value = 'error';
                  document.getElementById('showtoast').click();
                }
              );
            } else {
              var indice = peer.findIndex(function (element) {
                return element[1] == data.origin;
              });
              peer[indice][0].setRemoteDescription(
                new RTCSessionDescription(data.desc)
              );
            }
          }
        });
      }

      function muteClass() {
        var myButton = $('#muteMic');

        if (this.classList.contains('blue')) {
          localStream.getAudioTracks()[0].enabled = false;
          this.classList.remove('blue');
          this.classList.add('red');
          myButton.attr('data-original-title', 'Micrófono apagado');
          myButton.tooltip('show');
          setTimeout(function () {
            myButton.tooltip('hide');
            myButton.blur();
          }, 1000);
        } else {
          localStream.getAudioTracks()[0].enabled = true;

          this.classList.remove('red');
          this.classList.add('blue');
          myButton.attr('data-original-title', 'Micrófono encendido');
          myButton.tooltip('show');
          setTimeout(function () {
            myButton.tooltip('hide');
            myButton.blur();
          }, 1000);
        }
      }

      function convertFloat32ToInt16(buffer) {
        l = buffer.length;
        buf = new Int16Array(l);
        while (l--) {
          buf[l] = Math.min(1, buffer[l]) * 0x7fff;
        }
        return buf.buffer;
      }

      // function cerrarConexion() {}
      // function abrirConexion() {}

      //-------------------------------- Questionnaire ----------------------------------
      function inicializeSurvey() {
        totalStudents = 0;
        studentsWhoAnswered = 0;
        studentsWhoAnswerRight = 0;
        studentsWhoAnswerWrong = 0;
        studentOptions = {};
        document.getElementById('question-name').value = '';
        document.getElementById('option1').value = '';
        document.getElementById('option2').value = '';
        var sinresp = $('#option-sinresp');
        sinresp.attr('style', 'width: 100%');
        sinresp.attr('aria-valuenow', '100.00');
        sinresp.html('100.00%');
        var aa = $('#option-11');
        aa.attr('style', 'width: 20%');
        aa.attr('aria-valuenow', '0.00');
        aa.html('0.00%');
        var bb = $('#option-22');
        bb.attr('style', 'width: 20%');
        bb.attr('aria-valuenow', '0.00');
        bb.html('0.00%');
        var inc = $('#option-incorrect');
        inc.attr('style', 'width: 20%');
        inc.attr('aria-valuenow', '0.00');
        inc.html('0.00%');
        var c = $('#option-correct');
        c.attr('style', 'width: 20%');
        c.attr('aria-valuenow', '0.00');
        c.html('0.00%');
      }

      function sendQuestionnaire() {
        correctAns = [];
        var package = {};
        var button = $('#begin-questions');
        var question = document.getElementById('question-name').value;
        savedQuestionnaire.question = question;
        savedQuestionnaire.respond = {
          corrects: 0,
          incorrects: 0,
          norespond: totalStudents,
          option1: 0,
          option2: 0,
        };
        var option1 = document.getElementById('option1').value;
        savedQuestionnaire.option1 = { value: option1, isCorrect: false };
        var option2 = document.getElementById('option2').value;
        savedQuestionnaire.option2 = { value: option2, isCorrect: false };
        var otherQ = [];
        for (var i = 3; i <= numberOfExtraQuestions; i++) {
          var other = document.getElementById('option' + i).value;
          otherQ.push(other);
          savedQuestionnaire['option' + i] = { value: other, isCorrect: false };
          savedQuestionnaire.respond['option' + i] = 0;
        }
        var ban = true;
        otherQ.forEach(function (valor) {
          if (valor == '') ban = false;
        });

        var checks = $("input[name='correctAnswer']:checked");

        if (question == '') {
          button.attr('data-original-title', 'Debe escribir una pregunta');
          button.tooltip('show');
          setTimeout(function () {
            button.tooltip('hide');
            button.attr('data-original-title', '');
          }, 1500);

          return;
        } else if (question == '' || option1 == '' || option2 == '' || !ban) {
          button.attr(
            'data-original-title',
            'Falta escribir una o más opciones'
          );
          button.tooltip('show');
          setTimeout(function () {
            button.tooltip('hide');
            button.attr('data-original-title', '');
          }, 1500);
          return;
        } else if (checks.length == 0) {
          button.attr('data-original-title', 'No hay opciones correctas');
          button.tooltip('show');
          setTimeout(function () {
            button.tooltip('hide');
            button.attr('data-original-title', '');
          }, 1500);
          return;
        }

        for (var i = 0; i < checks.length; i++) {
          savedQuestionnaire['option' + checks[i].value].isCorrect = true;
          correctAns.push(checks[i].value);
        }

        package.question = question;
        package.option1 = savedQuestionnaire['option1'];
        package.option2 = savedQuestionnaire['option2'];
        for (var i = 3; i <= numberOfExtraQuestions; i++) {
          package['option' + i] = savedQuestionnaire['option' + i];
          $('#endR').before(
            '<div id="fatherOption' +
              i +
              '' +
              i +
              '"><div class="row"><div class="col-sm-12"><div class="form-group"><span class="input-group-addon">Respuesta ' +
              i +
              '</span><div id="option-' +
              i +
              '' +
              i +
              '" class="progress-bar progress-bar-default" role="progressbar" style="width: 20%" aria-valuenow="0.00" aria-valuemin="0" aria-valuemax="100"> 0.00%</div></div></div></div></div>'
          );
        }
        savedQuestionnaire.respond.norespond = totalStudents;
        socket.emit(events.questionnaire, package);
        $('#answer-panel').show();
        $('#modal-question').modal('hide');
        $('#startSurvey').hide();
        inicializeSurvey();
        cleanQuestionPanel();
        var sideTab = $('#sideTab');
        var results = $('#startResults');
        sideTab.append(
          '<li><a href="javascript:;" data-target="#moduloActividades" data-toggle="tab">Resultados<span class="span badge badge-success"></span></a></li>'
        );
        results.css('display', 'inline');
        sideTab.children().last().children().first().click();
        results.click();
      }

      function numberOfStudents(data) {
        totalStudents = data.students;
        $('#numStudents').html(totalStudents);
        if (role == 'server') {
          if (classStartTime == -1) {
            drawGraph('En espera', totalStudents);
          } else {
            Plotly.extendTraces(
              'chart',
              {
                x: [
                  [
                    secondsToTime(
                      Number(Date.now() - classStartTime)
                    ).toString(),
                  ],
                ],
                y: [[totalStudents]],
              },
              [0]
            );
          }
        }
      }

      function cleanAnswerPanel() {
        $('#questionTitle1').remove();
        $('#options').empty();
      }

      function receiveQuestionnaire(questionnaire) {
        cleanAnswerPanel();
        receivePackage = questionnaire;
        $('#questionTitle').append(
          '<span id="questionTitle1" style="word-wrap: break-word;">' +
            questionnaire.question.toUpperCase() +
            '</span>'
        );
        var options = $('#options');
        var i = 1;
        while (true) {
          if (questionnaire['option' + i])
            options.append(
              '<div class="row" style="display:flex;align-items:center;">\
                    <div class="col-sm-2">\
                    <input class="form-control" id="option' +
                i +
                '" type="checkbox" name="option" value="' +
                i +
                '">\
					</div>\
					<div class="col-sm-10">\
					<label style="word-wrap: break-word;" for="option' +
                i +
                '">' +
                i +
                ') ' +
                questionnaire['option' + i].value +
                '</label>\
					</div>\
					</div>'
            );
          else break;
          i++;
        }
        $('#modal-answer').modal({
          show: true,
        });
      }

      function endQuestionnaire() {
        var n = 2;
        while (true) {
          n++;
          if (document.getElementById('fatherOption' + n + '' + n)) {
            var elem = document.getElementById('fatherOption' + n + '' + n);
            elem.parentNode.removeChild(elem);
          } else {
            break;
          }
        }

        socket.emit(events.deleteWindow, {});
        socket.emit(events.saveQuestionnaire, savedQuestionnaire);
        savedQuestionnaire = {};
        $('#answer-panel').hide();
        $('#startSurvey').show();
        $('#sideTab').children().last().remove();
        $('#startResults').css('display', 'none');
        $('.page-quick-sidebar-toggler').first().click();
      }

      function sendAnswer() {
        var option = $('input[name="option"]:checked');

        if (option.length > 0) {
          var res = [];
          for (var i = 0; i < option.length; i++) {
            res.push(option[i].value);
          }
          socket.emit(events.answer, { answer: res });
          $('#modal-answer').modal('hide');
          var flag = true;
          res.forEach(function (element) {
            var filtered = Object.keys(receivePackage).filter(function (
              element1
            ) {
              if (/^option/.test(element1)) {
                if (receivePackage[element1].isCorrect == true) return true;
              }
              return false;
            });

            if (
              flag &&
              (!filtered.includes('option' + element) ||
                res.length != filtered.length)
            ) {
              flag = false;
              document.getElementById('result').innerText =
                'Respuesta Incorrecta';
              $('#modal-result').modal({ show: true });
            }
          });
          if (flag) {
            document.getElementById('result').innerText = 'Respuesta Correcta';
            $('#modal-result').modal({ show: true });
          }
        } else {
          var button = $('#answer');
          button.attr(
            'data-original-title',
            'Debe elegir al menos una respuesta'
          );
          button.tooltip('show');
          setTimeout(function () {
            button.tooltip('hide');
            button.attr('data-original-title', '');
          }, 1500);
        }
      }

      function receiveAnswer(answer) {
        studentsWhoAnswered++;
        var percentWhoNotAnswer = parseFloat(
          ((totalStudents - studentsWhoAnswered) / totalStudents) * 100
        ).toFixed(2);
        savedQuestionnaire.respond.norespond =
          totalStudents - studentsWhoAnswered;
        var sinresp = $('#option-sinresp');
        if (percentWhoNotAnswer > 20)
          sinresp.attr('style', 'width: ' + percentWhoNotAnswer + '%');
        else sinresp.attr('style', 'width: 20%');
        sinresp.attr('aria-valuenow', percentWhoNotAnswer);
        sinresp.html(percentWhoNotAnswer + '%');
        var i = 1;
        var bb = true;
        correctAns.forEach(function (element) {
          if (
            bb &&
            (!answer.answer.includes(element) ||
              correctAns.length != answer.answer.length)
          ) {
            bb = false;
            studentsWhoAnswerWrong++;
            var inc = $('#option-incorrect');
            var por = parseFloat(
              (studentsWhoAnswerWrong / studentsWhoAnswered) * 100
            ).toFixed(2);
            savedQuestionnaire.respond.incorrects = studentsWhoAnswerWrong;
            if (por > 20) inc.attr('style', 'width: ' + por + '%');
            else inc.attr('style', 'width: 20%');
            inc.attr('aria-valuenow', por);
            inc.html(por + '%');
            var c = $('#option-correct');
            var porr = parseFloat(100 - por).toFixed(2);
            if (porr > 20) c.attr('style', 'width: ' + porr + '%');
            else c.attr('style', 'width: 20%');
            c.attr('aria-valuenow', porr);
            c.html(porr + '%');
          }
        });
        if (bb) {
          studentsWhoAnswerRight++;
          var c = $('#option-correct');
          var porr = parseFloat(
            (studentsWhoAnswerRight / studentsWhoAnswered) * 100
          ).toFixed(2);
          savedQuestionnaire.respond.corrects = studentsWhoAnswerRight;
          if (porr > 20) c.attr('style', 'width: ' + porr + '%');
          else c.attr('style', 'width: 20%');
          c.attr('aria-valuenow', porr);
          c.html(porr + '%');
          var inc = $('#option-incorrect');
          var por = parseFloat(100 - porr).toFixed(2);
          if (por > 20) inc.attr('style', 'width: ' + por + '%');
          else inc.attr('style', 'width: 20%');
          inc.attr('aria-valuenow', por);
          inc.html(por + '%');
        }

        while (true) {
          if (!document.getElementById('option-' + i + '' + i)) break;

          var option = $('#option-' + i + '' + i);

          if (answer.answer.includes(i.toString())) {
            if (!studentOptions['option' + i]) studentOptions['option' + i] = 1;
            else
              studentOptions['option' + i] = studentOptions['option' + i] + 1;

            var porc = parseFloat(
              (studentOptions['option' + i] / studentsWhoAnswered) * 100
            ).toFixed(2);
            savedQuestionnaire.respond['option' + i] =
              studentOptions['option' + i];
            if (porc > 20) option.attr('style', 'width: ' + porc + '%');
            else option.attr('style', 'width: 20%');
            option.attr('aria-valuenow', porc);
            option.html(porc + '%');
          } else {
            if (!studentOptions['option' + i]) studentOptions['option' + i] = 0;
            var porc = parseFloat(
              (studentOptions['option' + i] / studentsWhoAnswered) * 100
            ).toFixed(2);
            savedQuestionnaire.respond['option' + i] =
              studentOptions['option' + i];
            if (porc > 20) option.attr('style', 'width: ' + porc + '%');
            else option.attr('style', 'width: 20%');
            option.attr('aria-valuenow', porc);
            option.html(porc + '%');
          }
          i++;
        }
        console.log(savedQuestionnaire);
      }

      function deleteQuestionnaire(data) {
        cleanAnswerPanel();
        $('#modal-answer').modal('hide');
      }

      //----------------------- End Questionnaire -------------------------------------
      function addQuestion() {
        numberOfExtraQuestions++;
        $('#addOption').append(
          '<div class="col-sm-10"><div class="input-group"><span class="input-group-addon">' +
            numberOfExtraQuestions +
            ')</span><input class="form-control" id="option' +
            numberOfExtraQuestions +
            '" type="text" placeholder="Escriba su respuesta aqui"></div></div><div class="col-sm-2"><input class="form-control" id="checkOption' +
            numberOfExtraQuestions +
            '" type="checkbox" name="correctAnswer" value="' +
            numberOfExtraQuestions +
            '"></div>'
        );
      }

      function cleanQuestionPanel() {
        if (numberOfExtraQuestions > 2) {
          while (true) {
            var r = document.getElementById('addOption');
            r.removeChild(r.lastChild);
            r.removeChild(r.lastChild);
            numberOfExtraQuestions--;
            if (numberOfExtraQuestions == 2) break;
          }
        }
        document.getElementById('question-name').value = '';
        document.getElementById('option1').value = '';
        document.getElementById('option2').value = '';
        $("input[name='correctAnswer']:checked").attr('checked', false);

        setTimeout(function () {
          $('#startSurvey').blur();
        }, 300);
      }

      function removeQuestion() {
        if (numberOfExtraQuestions > 2) {
          var r = document.getElementById('addOption');
          r.removeChild(r.lastChild);
          r.removeChild(r.lastChild);
          numberOfExtraQuestions--;
        }
      }
      //Manejo de eventos en Jquery por haber sido disparados con el framework

      function finishClass() {
        if (role == 'client') {
          $.get('/canVote', {
            _id: document.getElementById('class-id').value,
          })
            .done(function (puedeVotar) {
              if (puedeVotar) {
                if(timeFinishClass == 0)
                  timeFinishClass = Date.now()
                quieroSalir = true;
                $('#static').modal('show');
              } else {
                if (endClass == 1) {
                  quieroSalir = true;
                  sendVote();
                } else {
                  window.location.replace(
                    window.location.href.split('//')[0] +
                      '//' +
                      window.location.href.split('//')[1].split('/')[0] +
                      '/classesOnline'
                  );
                }
              }
            })
            .fail(function () {
              document.getElementById('message').innerText = 'No se pudo votar';
              document.getElementById('toastTypeGroup').value = 'error';
              document.getElementById('showtoast').click();
            });
        }else{
          if(classStartTime != -1)
          { 
            document.getElementById('duration').value = Date.now() - classStartTime
          }
          $('#formEndClass').submit()
        }
      }

      function copyURL(evt) {
        $('#shareButton').tooltip('show');
        var dummy = document.createElement('textarea');
        document.body.appendChild(dummy);
        dummy.value =
          window.location.href.split('//')[0] +
          '//' +
          window.location.href.split('//')[1].split('/')[0] +
          '/class/' +
          $('#class-id').val();
        dummy.select();
        document.execCommand('copy');
        document.body.removeChild(dummy);
        setTimeout(function () {
          $('#shareButton').tooltip('hide');
        }, 1000);
      }

      function sendVote() {
        let quitTime = 0;
          if ( classStartTime != -1){
              quitTime = timeFinishClass-connectionStartTime;
          }else{
            quitTime = 0;
          }
          console.log('datenow: ',Date.now(),', connectionStartTime: ',connectionStartTime)
        vote(
          document.getElementById('class-id').value,
          document.getElementById('input-vote').value,
          document.getElementById('voteOrigin').value,
          quitTime,
          1,
          endClass,
          document.getElementById('voteMethod').value
        );
      }

      function vote(classId, myVote, origin, quitTime, aVote, endC, method) {
        //obtener el quitTime Anterior
        if (!yavote) {
          $.post('/classVote', {
            _id: classId,
            vote: myVote,
            origin: origin,
            quitTime: quitTime,
            alreadyVote: aVote,
            endClass: endC,
            _method: method,
          })
            .done(function (data) {
              yavote = true;
              if (quieroSalir) {
                window.location.replace(
                  window.location.href.split('//')[0] +
                    '//' +
                    window.location.href.split('//')[1].split('/')[0] +
                    data
                );
              } else {
                $('#static').modal('hide');
              }
            })
            .fail(function () {
              document.getElementById('message').innerText = 'No se pudo votar';
              document.getElementById('toastTypeGroup').value = 'info';
              document.getElementById('showtoast').click();
            });
        } else {
          if (endClass == 1) {
            $.post('/classVote', {
              _id: classId,
              vote: myVote,
              origin: origin,
              quitTime: quitTime,
              alreadyVote: aVote,
              endClass: endC,
              _method: method,
            })
              .done(function (data) {
                window.location.replace(
                  window.location.href.split('//')[0] +
                    '//' +
                    window.location.href.split('//')[1].split('/')[0] +
                    data
                );
              })
              .fail(function () {
                document.getElementById('message').innerText = 'Error al salir';
                document.getElementById('toastTypeGroup').value = 'error';
                document.getElementById('showtoast').click();
              });
          } else {
            document.getElementById('message').innerText = 'Usted ya votó';
            document.getElementById('toastTypeGroup').value = 'info';
            document.getElementById('showtoast').click();
          }
        }
      }

      function voteWithoutQuit() {
        if (document.getElementById('voteButton')) {
          $.get('/canVote', {
            _id: document.getElementById('class-id').value,
          })
            .done(function (puedeVotar) {
              if (puedeVotar) {
                $('#static').modal('show');
                quieroSalir = false;
              } else {
                document.getElementById('message').innerText = 'Usted ya votó';
                document.getElementById('toastTypeGroup').value = 'info';
                document.getElementById('showtoast').click();
              }
            })
            .fail(function () {
              document.getElementById('message').innerText = 'Error al votar';
              document.getElementById('toastTypeGroup').value = 'error';
              document.getElementById('showtoast').click();
            });
        }
      }

      function uiEvents() {
        if (role == 'server') {
          $('.note-current-color-button-1').on(
            'primaryColorChange',
            changePencilColorPrimary
          );
          $('.note-current-color-button-2').on(
            'secondaryColorChange',
            changePencilColorSecondary
          );
          $('#pencilSize').on('change', changePencilSize);
          document.addEventListener('keydown', shortcuts);
          document
            .getElementsByClassName('note-saveButton')[0]
            .addEventListener('click', saveCanvas);
          document
            .getElementsByClassName('note-modeBotton')[0]
            .addEventListener('click', writingMode);
          document
            .getElementsByClassName('note-undoButton')[0]
            .addEventListener('click', sendUndo);
          document
            .getElementsByClassName('note-redoButton')[0]
            .addEventListener('click', sendRedo);

          document
            .getElementById('begin-questions')
            .addEventListener('click', sendQuestionnaire);
          document
            .getElementById('end-answers')
            .addEventListener('click', endQuestionnaire);
          document
            .getElementById('startClass')
            .addEventListener('click', startClass);
          document
            .getElementById('btnAddQuestion')
            .addEventListener('click', addQuestion);
          document
            .getElementById('cancelQuestionnaire')
            .addEventListener('click', cleanQuestionPanel);
          document
            .getElementById('btnRemoveQuestion')
            .addEventListener('click', removeQuestion);
          document
            .getElementById('muteMic')
            .addEventListener('click', muteClass);
          document
            .getElementById('startResults')
            .addEventListener('click', function () {
              $('#sideTab').children().last().children().first().click();
            });
        } else {
          document
            .getElementById('answer')
            .addEventListener('click', sendAnswer);
          document.getElementsByTagName('audio')[0].style.width =
            screenProportion * window.outerWidth + 'px';
          if (document.getElementById('voteStart'))
            document
              .getElementById('voteStart')
              .addEventListener('click', voteWithoutQuit);
        }
        var shareBtn = $('#shareButton');
        shareBtn.tooltip({
          placement: 'right',
          title: 'URL copiada',
          trigger: 'manual',
        });
        document
      .getElementById('finishButton')
      .addEventListener('click', finishClass);
      }
      document
        .getElementsByClassName('note-reviewButton')[0]
        .addEventListener('click', reviewRequest);
      document
        .getElementsByClassName('note-behindButton')[0]
        .addEventListener('click', toBehind);
      document
        .getElementsByClassName('note-aheadButton')[0]
        .addEventListener('click', toAhead);
      document
        .getElementById('messageTextBox')
        .addEventListener('keydown', sendMessage);
      document
        .getElementById('messageSendButton')
        .addEventListener('click', sendMessage);
      document
        .getElementById('startChat')
        .addEventListener('click', function () {
          $('#sideTab').children().first().children().first().click();
        });
      document.getElementById('shareButton').addEventListener('click', copyURL);
      if (document.getElementById('voteButton'))
        document
          .getElementById('voteButton')
          .addEventListener('click', sendVote);
      window.addEventListener('resize', onResizeEvent, true);
      //window.addEventListener('orientationchange',onResizeEvent);

      function focusOnTextbox() {
        document.getElementById('messageTextBox').focus();
      }
      var icons = document.getElementsByClassName(
        'icon socicon-btn socicon-btn-circle'
      );

      for (var i = 0; i < icons.length; i++) {
        icons[i].addEventListener('click', focusOnTextbox);
      }
      //if(document.getElementById('canvasImg'))
      //document.getElementById('canvasImg').addEventListener('change', function(){drawImageOnCanvas(this.files)});

      socket.on(events.newSegment, onNewSegment);
      socket.on(events.newLetter, onNewLetter);
      socket.on(events.saveScreen, cleanCanvas);
      socket.on(events.initialLoad, onInitialLoad);
      socket.on(events.newMessage, function (message) {
        onNewMessage(message, true);
      });
      socket.on(events.undo, onUndo);
      socket.on(events.redo, onRedo);
      socket.on(events.ratingRequest, onRatingRequest);
      socket.on(events.savedScreensRequest, onReview);
      socket.on(events.currentScreensRequest, onNewScreen);
      socket.on(events.questionnaire, receiveQuestionnaire);
      socket.on(events.numberOfStudents, numberOfStudents);
      socket.on(events.answer, receiveAnswer);
      socket.on(events.role, onRole);
      socket.on(events.deleteWindow, deleteQuestionnaire);
      socket.on(events.salte, function () {
        window.history.back();
      });
      socket.on(events.audio, function (data) {
        socket.emit(events.client, {});
      });
      socket.on(events.newScreen, function (data) {
        onNewScreen(data.drawings);
      });
      socket.on(events.classStartTime, function (data) {
        classStartTime = data.classStartTime;
        if (classStartTime != -1) {
            connectionStartTime = Date.now()
            document.getElementById('beginningMessageAlumno').style.display =
            'none';
            document.getElementById('canvasColumn').style.display = 'block';
          }
        })
     
      // socket.bindnod(eventos.abrirConexion, abrirConexion);
      // socket.bind(eventos.cerrarConexion, cerrarConexion);
    } else {
      document.getElementById('message').innerText =
        'Por favor actualice su navegador';
      document.getElementById('toastTypeGroup').value = 'error';
      document.getElementById('showtoast').click();
    }
  }

  // function Share() {
  //     var clipboard = new Clipboard('.btn');

  //     var shareButton = document.getElementById('shareButton');
  //     shareButton.addEventListener('click', notification);
  //     shareButton.setAttribute('data-clipboard-text', document.URL);

  //     function notification() {
  //         //for more notify options see http://bootstrap-notify.remabledesigns.com/

  //         $.notify({
  //             icon: 'fa fa-info-circle',
  //             title: '<strong>Enlace Copiado:</strong>',
  //             message: 'Pegue para compartir'
  //         }, {
  //                 animate: {
  //                     enter: 'animated flipInY',
  //                     exit: 'animated flipOutX'
  //                 },
  //                 placement: {
  //                     from: "top",
  //                     align: "right"
  //                 },
  //                 element: document.getElementsByClassName("portlet-title")[0]
  //             });
  //     }

  // }

  function drawGraph(x, y) {
    Plotly.react(
      'chart',
      [
        {
          type: 'scatter',
          x: [x],
          y: [y],
          mode: 'lines+markers',
        },
      ],
      {
        title: 'Cantidad de Alumnos',
        xaxis: {
          title: 'Tiempo',
        },
        yaxis: {
          title: 'Cantidad',
        },
      }
    );
  }

  $(document).ready(function () {
    //$( "#answer-panel" ).draggable();

    ComponentsEditors.init();
    InitWebSocket();
    const chart = document.getElementById('chart');
    if (chart) {
      drawGraph('En espera', 0);
    }

    // Share();
  });
})();
