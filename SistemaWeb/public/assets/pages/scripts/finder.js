(function() {
  var option = document.currentScript.getAttribute('saved');
  var path = '/classesOnline';
  $('.compartir').each(function() {
    $(this).tooltip({
      placement: 'right',
      title: 'URL copiada',
      trigger: 'manual'
    });
  });
  function shareURL() {
    var element = $(this).tooltip('show');
    var dummy = document.createElement('textarea');
    document.body.appendChild(dummy);
    dummy.value = $('.ingresar')
      .first()
      .prop('href');
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
    setTimeout(function() {
      element.tooltip('hide');
    }, 1000);
  }

  if (option == '1') path = '/classesRec';
  function armarElementos(data) {
    var classes = $('#classes');
    classes.empty();
    classes.append('</br>');

    if (data.dataClassesInprogres.length > 0) {
      data.dataClassesInprogres.forEach(function(clase, index) {
        var imagen;
        var classType = window.location.href.includes('classesRec')
          ? 'classRec'
          : 'class';
        if (data.dataOwners[index].userAvatar == null)
          imagen =
            '<img class="img-circle user-owner" src="/avatarsProfile/avatarUnknownSquare.jpg">';
        else
          imagen =
            '<img class="img-circle user-owner" src="' +
            data.dataOwners[index].userAvatar +
            '">';

        classes.append(
          '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">' +
            '<div class="portlet light portlet-fit">' +
            '<div class="portlet-title" style="word-wrap:break-word; display:block">' +
            '<div class="row">' +
            '<div class="mt-comments">' +
            '<div class="mt-comment">' +
            '<div class="mt-comment-img">' +
            imagen +
            '</div>' +
            '<div class="mt-comment-body" style="word-wrap:break-word; display:block">' +
            '<div class="mt-comment-info" style="word-wrap:break-word; display:block">' +
            '<span class="caption-subject font-green bold uppercase" href="/' +
            classType +
            '/' +
            clase._id +
            '">' +
            clase.classTitle +
            '</span>' +
            '</div>' +
            '<div class="mt-comment-text" style="word-wrap:break-word; display:block">' +
            ' Dictada por ' +
            data.dataOwners[index].userName +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="caption-desc font-grey-cascade class-description" style="word-wrap:break-word; display:block">' +
            clase.classDescription +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="portlet-body">' +
            '<div class="mt-element-overlay">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<div class="mt-overlay-1">' +
            '<img src="' +
            clase.classImg +
            '">' +
            '<div class="mt-overlay">' +
            '<ul class="mt-info">' +
            '<li><i>' +
            '<a class="btn default btn-outline ingresar" href="/' +
            classType +
            '/' +
            clase._id +
            '">' +
            '<i class="icon-login"></i>' +
            '</a></i>' +
            '</li>' +
            '<li><i>' +
            '<a class="btn default btn-outline compartir">' +
            '<i class="icon-share"></i>' +
            '</a></i>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );
      });

      $('.ingresar').each(function() {
        $(this)
          .parent()
          .tooltip({
            placement: 'top',
            title: 'Ingresar'
          });
      });
      $('.compartir').each(function() {
        $(this)
          .parent()
          .tooltip({
            placement: 'top',
            title: 'Copiar URL'
          });
      });
      $('.compartir').each(function() {
        $(this).tooltip({
          placement: 'right',
          title: 'URL copiada',
          trigger: 'manual'
        });
      });

      $('.compartir').on('click', shareURL);
    } else {
      classes.append(
        '<h4 class="text-center">No se encontraron resultados</h4>'
      );
    }
  }
  function search() {
    var query = {};

    query.classCategory = $('#classCategory option:selected').val();

    var classTitle = $('#className').val();
    if (classTitle != '') query.classTitle = classTitle;

    var classOwner = $('#classOwner').val();
    if (classOwner != '') query.classOwner = classOwner;

    $.post(path, query, function(data) {
      var paginator = $('#paginator').empty();
      if (data.dataClassesInprogres.length > 0) {
        var extra = '';
        for (var i = 1; i <= data.pages; i++) {
          if (i == data.page) {
            extra +=
              '<li class="page-item active">' +
              '<a class="page-link pages">' +
              i +
              '</a>' +
              '</li>';
          } else {
            extra +=
              '<li class="page-item">' +
              '<a class="page-link pages">' +
              i +
              '</a>' +
              '</li>';
          }
        }
        paginator.append(
          '<nav>' + '<ul class="pagination">' + extra + '</ul>' + '</nav>'
        );
      }
      armarElementos(data);
    });
  }

  $('.form-control').on('keyup', search);
  $('#classCategory').on('click', search);

  $(document).on('click', '.pages', function(data) {
    var query = {};

    query.classCategory = $('#classCategory option:selected').val();

    var classTitle = $('#className').val();
    if (classTitle != '') query.classTitle = classTitle;

    var classOwner = $('#classOwner').val();
    if (classOwner != '') query.classOwner = classOwner;

    query.page = data.currentTarget.innerHTML;

    $.post(path, query, function(data1) {
      $('.pages')
        .parent()
        .removeClass('active');
      data.target.parentNode.classList.add('active');
      armarElementos(data1);
    });
  });

  $('.compartir').on('click', shareURL);
})();
