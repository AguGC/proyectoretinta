document.addEventListener('DOMContentLoaded', function (event) {
    console.log('hola')
    function urlify(text) {
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return text.replace(urlRegex, function (url) {
            return '<a href="' + url + '">' + url + '</a>';
        })

    }

    var about = document.getElementById('myAbout');
    if (about) {
        console.log('Lo que esta en about:' + about.innerText)
        about.innerHTML = urlify(about.innerText);
        console.log('Lo que esta en about finalizado:' + urlify(about.innerText))
    }


})

