var UIToastr = function() {

    return {
        //main function to initiate the module
        init: function() {

            var i = -1,
                toastCount = 0,
                $toastlast

            $('#showtoast').click(function() {
                var shortCutFunction = $("#toastTypeGroup").val();
                var msg = $('#message').val();
                var title = $('#title').val() || '';
                var toastIndex = toastCount++;

                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass: 'toast-top-right',
                    onclick: null
                };

                var $toast = toastr[shortCutFunction](msg, title);
                $toastlast = $toast;
            });
        }
    };
}();

jQuery(document).ready(function() {
    UIToastr.init();
    if ($('#message').val().length > 0) {
        $('#showtoast').click();
    }
});