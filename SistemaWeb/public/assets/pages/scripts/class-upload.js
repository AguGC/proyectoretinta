jQuery(document).ready(function () {
  var shareBtn = $('#shareButton');
  shareBtn.tooltip({
    placement: 'top',
    title: 'URL copiada',
    trigger: 'manual'
  });
  var quieroSalir = true;
  var canvas = $('#canvas'),
    audioTag = $('#audio'),
    screenProportion = 0.85;
  audioTag.css('width', screenProportion * window.outerWidth);
  canvas.attr('height', screen.height);
  canvas.attr('width', screen.width);
  canvas.css('height', screenProportion * window.outerHeight);
  canvas.css('width', screenProportion * window.outerWidth);
  canvas.css('border', '1px solid #8a8a8a');
  canvas.css('border-radius', '3px');
  canvas.css('background-color', 'white');

  let audio = document.getElementById('audio'),
    wasPaused = false;

  audio.addEventListener('error', function (e) {
    document.getElementById('message').innerText = 'Falló la reproducción';
    document.getElementById('toastTypeGroup').value = 'error';
    document.getElementById('showtoast').click();
  });
  AudioSynchronizer.setChronometer(audio);
  audio.onplay = function () {
    //if (audio.readyState === 4) {
    AudioSynchronizer.draw();
    // } else {
    //     alert("Still Loading - Please wait");
    // }
  };
  audio.onseeking = function () {
    AudioSynchronizer.stopDrawing();
    audio.pause();
  };
  audio.onpause = function () {
    AudioSynchronizer.stopDrawing();
    wasPaused = true;
  };
  audio.onseeked = function () {
    AudioSynchronizer.continueDrawing();
  };

	/*if(document.getElementById('valueButton')){
		document.getElementById('valueButton').addEventListener('click', function(data){
			$('#static').modal('show')
		});
    }*/

  function finishClass() {
    if (document.getElementById('voteButton')) {
      $.get('/canVote', {
        _id: document.getElementById('class-id').value
      })
        .done(function (puedeVotar) {
          if (puedeVotar) {
            $('#static').modal('show');
            quieroSalir = true;
          }
          else {
            window.location.replace(
              window.location.href.split('//')[0] +
              '//' +
              window.location.href.split('//')[1].split('/')[0] +
              '/classesRec'
            );
          }
        })
        .fail(function () {
          document.getElementById('message').innerText =
            'No se pudo votar';
          document.getElementById('toastTypeGroup').value = 'error';
          document.getElementById('showtoast').click();

        });

    } else {
      window.location.replace(
        window.location.href.split('//')[0] +
        '//' +
        window.location.href.split('//')[1].split('/')[0] +
        '/myClasses'
      );
    }

  }

  function copyURL(evt) {
    $('#shareButton').tooltip('show');
    var dummy = document.createElement('textarea');
    document.body.appendChild(dummy);
    dummy.value =
      window.location.href.split('//')[0] +
      '//' +
      window.location.href.split('//')[1].split('/')[0] +
      '/classRec/' +
      $('#class-id').val();
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
    setTimeout(function () {
      $('#shareButton').tooltip('hide');
    }, 1000);
  }

  function sendVote() {
    vote(
      document.getElementById('class-id').value,
      document.getElementById('input-vote').value,
      document.getElementById('voteOrigin').value,
      null,
      1,
      null,
      document.getElementById('voteMethod').value
    );
  }

  function vote(classId, myVote, origin, quitTime, aVote, endC, method) {

    $.post('/classVote', {
      _id: classId,
      vote: myVote,
      origin: origin,
      quitTime: quitTime,
      alreadyVote: aVote,
      endClass: endC,
      _method: method
    })
      .done(function (data) {

        if (quieroSalir) {
          window.location.replace(
            window.location.href.split('//')[0] +
            '//' +
            window.location.href.split('//')[1].split('/')[0] +
            data
          );
        } else {
          $('#static').modal('hide');
        }

      })
      .fail(function () {
        document.getElementById('message').innerText =
          'No se pudo votar';
        document.getElementById('toastTypeGroup').value = 'error';
        document.getElementById('showtoast').click();

      });

  }

  function voteWithoutQuit() {
    if (document.getElementById('voteButton')) {
      $.get('/canVote', {
        _id: document.getElementById('class-id').value
      })
        .done(function (puedeVotar) {
          if (puedeVotar) {
            $('#static').modal('show');
            quieroSalir = false;
          }
          else {
            document.getElementById('message').innerText =
              'Usted ya votó';
            document.getElementById('toastTypeGroup').value = 'info';
            document.getElementById('showtoast').click();
          }

        })
        .fail(function () {
          document.getElementById('message').innerText =
            'No se pudo votar';
          document.getElementById('toastTypeGroup').value = 'error';
          document.getElementById('showtoast').click();

        })

    }
  }

  document.getElementById('shareButton').addEventListener('click', copyURL);
  document.getElementById('finishButton').addEventListener('click', finishClass);
  if (document.getElementById('voteButton'))
    document.getElementById('voteButton').addEventListener('click', sendVote);
  if (document.getElementById('voteStart'))
    document.getElementById('voteStart').addEventListener('click', voteWithoutQuit)

});
