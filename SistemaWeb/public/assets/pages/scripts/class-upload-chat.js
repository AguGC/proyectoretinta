$(document).ready(function () {

	var myChat = $('.myChat')[0],
		onResizeEvent = function () {
			$('.panel-body .myChat').css('height', window.innerHeight / 2 + 'px');
		};
	onResizeEvent();

	var onNewMessage = function (message) {
		//Funcion conversion milisegundos de tiempo en horas minutos y segundos
		var secondsToTime = function (s) {

			function addZ(n) {
				return (n < 10 ? '0' : '') + n;
			}

			var ms = s % 1000;
			s = (s - ms) / 1000;
			var secs = s % 60;
			s = (s - secs) / 60;
			var mins = s % 60;
			var hrs = (s - mins) / 60;

			return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
		}
		var tiempoMilisegundo = `${message.timeMsg}`;

		var tiempoHoras = (tiempoMilisegundo != -1) ? secondsToTime(Number(tiempoMilisegundo)) : 'Sala de Espera';
		//fin funcion conversion

		var li = document.createElement('li');
		li.setAttribute('class', 'left clearfix');
		var highlighted;
		//console.log('message --: '+JSON.stringify(message));
		//p.style.color = message.color;
		if (message.server) {
			highlighted = `<p class="text-danger">${message.text}</p>`;
			message.user += " (DOCENTE)"
		}
		else
			highlighted = `<p>${message.text}</p>`;
		var items = [
			`<span class="chat-img pull-left">`,
			`<img class="img-circle-chat" src="/avatarsProfile/${message.id}" onerror="this.src='/avatarsProfile/avatarUnknownSquare.jpg';">`,
			`</span>`,
			`<div class="chat-body clearfix">`,
			`<div class="header">`,
			`<strong class="primary-font">`,
			`<a href ="/myClasses:${message.mail}" target="_blank" rel="noopener noreferrer">${message.user}</a>`,
			`</strong>`,
			`<small class="pull-right text-muted">`,
			`<span class="glyphicon glyphicon-time">`,
			`</span>`,
			tiempoHoras,
			`</small>`,
			`</div>`,
			highlighted,
			`</div>`,
			`</li>`,
		];
		li.innerHTML = items.join('');
		document.getElementById('chat').appendChild(li);
		myChat.scrollTop = '0px';
	},
		loadChat = function (messages) {
			for (i = 0; i < messages.length; i++) {
				onNewMessage(messages[i])
			}
		},
		request = $.post('/savedChat', { 'classId': $('#classId').val() })
			.done(function (data) {
				console.log('chat received');
				loadChat(data.classChat);
			})
			.fail(function () {
				console.log('error in saved chat request');
			})
	window.addEventListener("resize", onResizeEvent, true);
});