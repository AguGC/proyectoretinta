var ComponentsNoUiSliders = function() {

    var demo2 = function() {
        var connectSlider = document.getElementById('demo2');

        noUiSlider.create(connectSlider, {
            start: [20],
            connect: false,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            demo2();
        }
    };

}();

jQuery(document).ready(function() {
    ComponentsNoUiSliders.init();
});