$(".class-register").validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false,
    rules:
    {
        title:
        {
            required:true,
            minlength:3,
            maxlength:60
        },
        description:
        {
            required:true,
            minlength:3,
            maxlength:100

        },
        category:
        {
            required:true
        },
        password:
        {
            required:'#checkbox1_1:checked',
            minlength:8,
            maxlength:16
        }
    },
    invalidHandler: function (event, validator) { //display error alert on form submit   
        var errors = validator.numberOfInvalids();
        if (errors) {                    
            validator.errorList[0].element.focus();
        }
     },
    highlight: function (element) { // hightlight error inputs
        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        $(element).siblings('span.help-block').hide();
    },

    success: function (label) {
        $(label).siblings('span.help-block').show();
        label.closest('.form-group').removeClass('has-error');
        label.remove();
        
    },
    submitHandler: function (form) {
        form.submit();
    }
});
$('.class-register input').bind('keyup', function (e) {
    if ( e.which == 13) {
        if ($('.class-register').validate().form()) {
            $('.class-register').submit();
        }
        return false;
    }
});