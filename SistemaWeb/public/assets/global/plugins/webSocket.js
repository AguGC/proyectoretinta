function MiWebSocket(url) {
    var conn = new WebSocket(url);

    var callbacks = {};

    this.bind = function(eventName, callback) {
        callbacks[eventName] = callbacks[eventName] || [];
        callbacks[eventName].push(callback);
        return this; // chainable
    };

    this.send = function(eventName, eventData) {
        var json = JSON.stringify({
            event: eventName,
            data: eventData
        });
        conn.send(json); // <= envia la informacion en JSON al socket server
        return this;
    };

    // despacha los eventos a los correctos manejadores
    conn.onmessage = function(evt) {
        var json = JSON.parse(evt.data)
        dispatch(json.event, json.data)
    };

    conn.onclose = function() {
        //        dispatch(eventos.cerrarConexion, null)
    }
    conn.onopen = function() {
        //        dispatch(eventos.abrirConexion, null)
    }

    window.onunload = function() {
        conn.close();
    }

    var dispatch = function(eventName, message) {
        var chain = callbacks[eventName];
        if (typeof chain == 'undefined') return; // no hay callbacks para este evento
        for (var i = 0; i < chain.length; i++) {
            chain[i](message)
        }
    }
};