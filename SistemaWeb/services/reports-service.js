/**
 * @file Servicio para administrar reportes
 * @name reports-service
 * @requires path,class-model
 * @author ReTinta
 * @version 1.0.0
 */
'use strict';

const path = require('path'),
  ClassModel = require(path.join(__dirname, '..', 'models', 'class-model'));

/**
 * @class
 * @memberof reports-service
 */
class ReportService {
  /**
   * @function constructor() - Crea el objeto de clase ReportService
   * @memberof reports-service.ReportService
   * @instance
   */
  constructor() {}

  /**
   * @function obtenerReportes() - Permite obtener informacion
   * sumarizada de las clases de un owner filtrado por ids de clases o no
   * @param {string} idOwner - Id de owner
   * @param {string[]} arrayIds - Arrays con id de clases
   * @memberof reports-service.ReportService
   * @instance
   */
  async obtenerReportes(idOwner, arrayIds) {
    let arrayClasses = await ClassModel.getClassesByOwnerId(idOwner);
    if (arrayIds) {
      arrayClasses = this.filterForIds(arrayClasses, arrayIds);
    }
    const asistencias = [];
    const classesIds = [];
    const [
      classCantE,
      classCantMB,
      classCantB,
      classCantR,
      classCantM,
      cantCorrectas,
      cantIncorrectas,
      cantNorespondidas
    ] = this.summarization(arrayClasses);
    let assisTotal = 0;
    let cantidadVotantesTotalizador = 0;
    let cantidadPermanenciaTotalizador = 0;
    arrayClasses.forEach(myClass => {
      asistencias.push({
        claseName: myClass.classTitle,
        cantidadAsistencias: myClass.classAssists.length,
        id: myClass.id
      });
      classesIds.push(myClass.id);

      cantidadVotantesTotalizador += myClass.classAssists.reduce(
        (acc, current) => {
          acc += current.alreadyVote == 1 ? 1 : 0;
          return acc;
        },
        0
      );

      cantidadPermanenciaTotalizador += myClass.classAssists.reduce(
        (acc, current) => {
          acc += current.endClass == 1 ? 1 : 0;
          return acc;
        },
        0
      );
      assisTotal += myClass.classAssists.length;
    });
    let porcentajeVotantes = 0;
    let porcentajePermanencia = 0;
    if (assisTotal != 0) {
      porcentajeVotantes = (cantidadVotantesTotalizador / assisTotal) * 100;
      porcentajePermanencia =
        (cantidadPermanenciaTotalizador / assisTotal) * 100;
    }

    return {
      valoracion: {
        cantidadE: classCantE,
        cantidadMB: classCantMB,
        cantidadB: classCantB,
        cantidadR: classCantR,
        cantidadM: classCantM
      },
      cuestionarios: {
        cantidadCorrectas: cantCorrectas,
        cantidadIncorrectas: cantIncorrectas,
        cantidadNoRespondidas: cantNorespondidas
      },
      asistencia: asistencias,
      totalVistas: assisTotal,
      porcentajeVotantes: porcentajeVotantes.toFixed(2),
      porcentajePermanencia: porcentajePermanencia.toFixed(2),
      classesIds: classesIds
    };
  }

  /**
   * @function filterForIds() - Filtra vector de clases segun
   * ids proporcionadas
   * @param {Object[]} arrayClasses - Array de clases
   * @param {string[]} arrayIds - Arrays con id de clases
   * @memberof reports-service.ReportService
   * @instance
   */
  filterForIds(arrayClasses, arrayIds) {
    return arrayClasses.filter(myClass => {
      return arrayIds.includes(myClass.id);
    });
  }
  /**
   * @function summarization() - Permite obtener informacion sumarizada
   * de las clases proporcionadas
   * @param {Object[]} arrayClasses - Arrays de clases
   * @memberof reports-service.ReportService
   * @instance
   */
  summarization(arrayClasses) {
    let summarizationQuestions = arrayQuestions => {
      let [cantCorrectas, cantIncorrectas, cantNoRespondidas] = [0, 0, 0];
      if (arrayQuestions && arrayQuestions.length > 0) {
        const accumPreguntas = arrayQuestions.reduce(function(
          preguntaAnterior,
          preguntaActual,
          indice,
          vector
        ) {
          preguntaActual.respond.corrects =
            preguntaAnterior.respond.corrects + preguntaActual.respond.corrects;
          preguntaActual.respond.incorrects =
            preguntaAnterior.respond.incorrects +
            preguntaActual.respond.incorrects;
          preguntaActual.respond.norespond =
            preguntaAnterior.respond.norespond +
            preguntaActual.respond.norespond;
          return preguntaActual;
        });
        return [
          accumPreguntas.respond.corrects,
          accumPreguntas.respond.incorrects,
          accumPreguntas.respond.norespond
        ];
      }

      return [cantCorrectas, cantIncorrectas, cantNoRespondidas];
    };
    let summaries;
    if (arrayClasses.length > 1) {
      summaries = arrayClasses.reduce(function(
        classAnterior,
        classActual,
        indice,
        vector
      ) {
        classActual.classCantE =
          classAnterior.classCantE + classActual.classCantE;
        classActual.classCantMB =
          classAnterior.classCantMB + classActual.classCantMB;
        classActual.classCantB =
          classAnterior.classCantB + classActual.classCantB;
        classActual.classCantR =
          classAnterior.classCantR + classActual.classCantR;
        classActual.classCantM =
          classAnterior.classCantM + classActual.classCantM;
        let [antCorrectas, antIncorrectas, antNoRespondidas] = [
          classAnterior.classCorrects,
          classAnterior.classIncorrects,
          classAnterior.classNoRespond
        ];
        if (indice == 1) {
          [
            antCorrectas,
            antIncorrectas,
            antNoRespondidas
          ] = summarizationQuestions(classAnterior.classQuestionnaires);
        }

        let [
          actCorrectas,
          actIncorrectas,
          actNoRespondidas
        ] = summarizationQuestions(classActual.classQuestionnaires);

        classActual.classCorrects = antCorrectas + actCorrectas;
        classActual.classIncorrects = antIncorrectas + actIncorrectas;
        classActual.classNoRespond = antNoRespondidas + actNoRespondidas;

        return classActual;
      });
    } else {
      summaries = arrayClasses[0];
      let [
        cantCorrectas,
        cantIncorrectas,
        cantNoRespondidas
      ] = summarizationQuestions(arrayClasses[0].classQuestionnaires);
      summaries.classCorrects = cantCorrectas;
      summaries.classIncorrects = cantIncorrectas;
      summaries.classNoRespond = cantNoRespondidas;
    }

    return [
      summaries.classCantE,
      summaries.classCantMB,
      summaries.classCantB,
      summaries.classCantR,
      summaries.classCantM,
      summaries.classCorrects,
      summaries.classIncorrects,
      summaries.classNoRespond
    ];
  }

  // {
  //     "valoracion": {
  //         "cantidadE": 123,
  //         "cantidadMB": 312,
  //         "cantidadB": 4123,
  //         "cantidadR": 3,
  //         "cantidadM": 1
  //     },
  //     "cuestionarios": {
  //         "cantidadIncorrectas": 123,
  //         "cantidadCorrectas": 312,
  //         "cantidadNoRespondidas": 3
  //     },
  //     "asistencia": [{
  //         "claseName": "claseLoca", // Eje X Grafico de Barras
  //         "cantidadAsistentes": 12  // Eje y Grafico de Barras
  //     }]
  // }
}

module.exports = ReportService;
