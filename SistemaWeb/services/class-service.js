/**
 * @file Servicio para administrar las pantallas de las clases
 * @name class-service
 * @requires path,class-model
 * @author ReTinta
 * @version 1.0.0 
 */
'use strict'

const path = require('path'),
    ClassModel = require(path.join(__dirname, '..', 'models', 'class-model'));

/**
 * @class
 * @memberof class-service
 */
let ClassService = () => {
};

/**
 * @function getScreens() - Permite obtener las pantallas
 * de una clase 
 * @param {string} classId - Id de clase
 * @memberof class-service.ClassService
 * @instance
 */
ClassService.getScreens = async function (classId) {
    let myClass = await ClassModel.findOne(classId)
        if (myClass.classScreens) {
            console.log(myClass.classScreens);
            return myClass.classScreens;
        }else {
            return null
        }
};


module.exports = ClassService;