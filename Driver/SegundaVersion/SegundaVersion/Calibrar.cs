﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging;
using SegundaVersion.Properties;

namespace SegundaVersion
{
    public partial class Calibrar : Form
    {
        int estadoCal = 0;
        Point[] puntos = new Point[4];
        private FilterInfoCollection cameras;
        private VideoCaptureDevice myCamera;
        DateTime ahora = DateTime.Now;
        TimeSpan duracion = new TimeSpan(0, 0, 3);
        RecursiveBlobCounter blob = new RecursiveBlobCounter();
        //BlobCounter blob = new BlobCounter();
        Rectangle[] recs = null;
        TrayMinimizerForm f = null;
        System.Media.SoundPlayer simpleSound = new System.Media.SoundPlayer(Directory.GetCurrentDirectory() + "\\Audio\\Checkmark.wav");
        public Calibrar()
        {
            InitializeComponent();
        }

        private void Calibrar_Load(object sender, EventArgs e)
        {

            blanco1.Height = blanco1.BackgroundImage.Height;
            blanco1.Width = blanco1.BackgroundImage.Width;

            int anchoS = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width;
            int largoS = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Height;
            Size = new Size(anchoS, largoS);
            int locPanelX = (anchoS / 2) - (panel1.Size.Width / 2);
            int locPanelY = (largoS / 2) - (panel1.Size.Height / 2);
            FormBorderStyle = FormBorderStyle.None;

            panel1.Location = new Point(locPanelX, locPanelY);

            blanco2.Height = blanco2.BackgroundImage.Height;
            blanco2.Width = blanco2.BackgroundImage.Width;
            blanco2.Location = new Point((anchoS - blanco2.Height), (blanco2.Width / 2));

            blanco3.Height = blanco3.BackgroundImage.Height;
            blanco3.Width = blanco3.BackgroundImage.Width;
            blanco3.Location = new Point((blanco3.Height / 2), (largoS - blanco3.Width));

            blanco4.Height = blanco4.BackgroundImage.Height;
            blanco4.Width = blanco4.BackgroundImage.Width;
            blanco4.Location = new Point((anchoS - blanco4.Height), (largoS - blanco4.Width));
            cameras = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            blob.MinWidth = 3;
            blob.MinHeight = 3;
            blob.FilterBlobs = false;
            blob.ObjectsOrder = ObjectsOrder.Size;
            blob.BackgroundThreshold = Color.FromArgb(180, 180, 180);
            myCamera = new VideoCaptureDevice(cameras[0].MonikerString);

            //myCamera.DisplayPropertyPage(IntPtr.Zero);
            myCamera.VideoResolution = myCamera.VideoCapabilities[0];
           // Console.WriteLine("Mi resolución es: "+myCamera.VideoResolution);

            myCamera.NewFrame += new NewFrameEventHandler(myCamera_newFrame);
            myCamera.Start();


        }

        private void resetearPuntos()
        {
            estadoCal = 0;
            this.Invoke((MethodInvoker)delegate { txtMostrar.Text = ""; });
            this.Invoke((MethodInvoker)delegate { blanco2.Visible = false; });
            this.Invoke((MethodInvoker)delegate { blanco3.Visible = false; });
            this.Invoke((MethodInvoker)delegate { blanco4.Visible = false; });
            blanco1.BackgroundImage = Resources.blancoDisparo40x40;
            blanco2.BackgroundImage = Resources.blancoDisparo40x40;
            blanco3.BackgroundImage = Resources.blancoDisparo40x40;
            blanco4.BackgroundImage = Resources.blancoDisparo40x40;


        }

        private void myCamera_newFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap imageF = (Bitmap)eventArgs.Frame.Clone();
            blob.ProcessImage(imageF);   // cuando ejecuta esta linea tira la Exepción //
            recs = blob.GetObjectsRectangles();
            if (recs.Length > 0)
            {

                Rectangle re = recs[0];
                int x = re.X + (re.Width / 2);
                int y = re.Y + (re.Height / 2);


                if (DateTime.Now - ahora >= duracion)
                {
                    switch (estadoCal)
                    {
                        case 0:


                            if (x > imageF.Size.Width * 0.50 || y > imageF.Size.Height * 0.50)
                            {
                                MessageBox.Show("ERROR: Revise que vertice toco o posicione mejor la camara");
                                resetearPuntos();
                            }
                            else
                            {
                                puntos[0] = new Point(x, y);
                                this.Invoke((MethodInvoker)delegate { txtMostrar.Text = "X: " + x + " , Y: " + y; });
                                estadoCal++;
                                blanco1.BackgroundImage = Resources.pasa40x40;
                                simpleSound.Play();
                                this.Invoke((MethodInvoker)delegate { blanco2.Visible = true; });
                            }
                            ahora = DateTime.Now;
                            break;
                        case 1:

                            if (x <= puntos[0].X)
                            {
                                MessageBox.Show("ERROR: Revise que vertice toco o posicione mejor la camara");
                                resetearPuntos();
                            }
                            else
                            {
                                puntos[1] = new Point(x, y);
                                this.Invoke((MethodInvoker)delegate { txtMostrar.Text = "X: " + x + " , Y: " + y; });
                                estadoCal++;
                                simpleSound.Play();
                                blanco2.BackgroundImage = Resources.pasa40x40;
                                this.Invoke((MethodInvoker)delegate { blanco3.Visible = true; });
                            }
                            ahora = DateTime.Now;
                            break;

                        case 2:


                            if (y <= puntos[0].Y)
                            {
                                MessageBox.Show("ERROR: Revise que vertice toco o posicione mejor la camara");
                                resetearPuntos();
                            }
                            else
                            {
                                puntos[2] = new Point(x, y);
                                this.Invoke((MethodInvoker)delegate { txtMostrar.Text = "X: " + x + " , Y: " + y; });
                                blanco3.BackgroundImage = Resources.pasa40x40;
                                estadoCal++;
                                simpleSound.Play();
                                this.Invoke((MethodInvoker)delegate { blanco4.Visible = true; });
                            }
                            ahora = DateTime.Now;
                            break;
                        case 3:


                            if (x <= puntos[3].X || y <= puntos[1].Y)
                            {
                                MessageBox.Show("ERROR: Revise que vertice toco o posicione mejor la camara");
                                resetearPuntos();
                            }
                            else
                            {
                                puntos[3] = new Point(x, y);
                                blanco4.BackgroundImage = Resources.pasa40x40;
                                estadoCal++;
                                simpleSound.Play();
                                f.setPuntos(puntos);
                                f.habilitarStart(true);
                                myCamera.SignalToStop();
                                myCamera = null;
                                this.Invoke((MethodInvoker)delegate { this.Close(); });

                            }
                            ahora = DateTime.Now;
                            break;
                    }
                }

            }

        }
        internal void SPrincipal(ref TrayMinimizerForm form)
        {
            this.f = form;
        }

        
    }
}
