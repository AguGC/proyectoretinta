﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging;
using System.Runtime.InteropServices;
using System.IO.Ports;

namespace SegundaVersion
{
    public partial class TrayMinimizerForm : Form
    {

        public TrayMinimizerForm()
        {
            InitializeComponent();
         

           
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
            Application.Exit();
        }

        String numDev = "";
        Point[] puntos = new Point[4];
        private FilterInfoCollection cameras;
        private VideoCaptureDevice myCamera;
        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        [DllImport("user32.dll")]
        private static extern bool SetCursorPos(int x, int y);
        bool flagF = false;
        int anchoP = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width;
        int largoP = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Height;
        short propX;
        short propY;
        RecursiveBlobCounter blob = new RecursiveBlobCounter();
        //BlobCounter blob = new BlobCounter();
        Rectangle[] recs = null;
        short[] XRef;
        short[] YRef;
        int avx = 0;
        int avy = 0;
        int a = 0;
        private void btnCalibrar_Click(object sender, EventArgs e)
        {
            Calibrar calibrar = new Calibrar();
            TrayMinimizerForm form = this;
            calibrar.SPrincipal(ref form);
            calibrar.Show();

        }

        public void setPuntos(Point[] p)
        {
            puntos = p;
            int ancho = puntos[1].X - puntos[0].X;
            int largo = puntos[2].Y - puntos[0].Y;
            propX = (short)Math.Round(((float)anchoP) / ancho, 0);
            propY = (short)Math.Round(((float)largoP) / largo, 0);

            float ax = ((float)puntos[0].Y - puntos[1].Y) / (puntos[0].X - puntos[1].X);
            float bx = ((float)puntos[1].X * puntos[0].Y - puntos[0].X * puntos[1].Y) / (puntos[1].X - puntos[0].X);
            float ay = ((float)puntos[0].X - puntos[2].X) / (puntos[0].Y - puntos[2].Y);
            float by = ((float)puntos[2].Y * puntos[0].X - puntos[0].Y * puntos[2].X) / (puntos[2].Y - puntos[0].Y);
            YRef = new short[puntos[1].X + 501];
            XRef = new short[puntos[2].Y + 501];
            for (int i = 0; i <= puntos[1].X + 500; i++)
            {
                YRef[i] = (short)Math.Round((ax * i + bx), 0);
            }
            for (int j = 0; j <= puntos[2].Y + 500; j++)
            {
                XRef[j] = (short)Math.Round((ay * j + by), 0);
            }
        }

        public void habilitarStart(bool ban)
        {
            this.Invoke((MethodInvoker)delegate { btnIniciar.Enabled = ban; });

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
            notifyIcon1.BalloonTipText = "App Maximizada";
            notifyIcon1.ShowBalloonTip(500);
            cameras = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            blob.MinWidth = 3;
            blob.MinHeight = 3;
            blob.FilterBlobs = false;
            blob.ObjectsOrder = ObjectsOrder.Size;
            blob.BackgroundThreshold = Color.FromArgb(180, 180, 180);
            myCamera = new VideoCaptureDevice(cameras[0].MonikerString);
            myCamera.VideoResolution = myCamera.VideoCapabilities[0];
            //cfcfConsole.WriteLine("Mi resolución es: " + myCamera.VideoResolution);
            //myCamera.DisplayPropertyPage(IntPtr.Zero);
            myCamera.NewFrame += new NewFrameEventHandler(myCamera_nFrame);
            myCamera.Start();

            this.WindowState = FormWindowState.Minimized;

        }

        private void myCamera_nFrame(object sender, NewFrameEventArgs eventArgs)
        {
            blob.ProcessImage((Bitmap)eventArgs.Frame.Clone());
            recs = blob.GetObjectsRectangles();
            if (recs.Length > 0)
            {
                avx += (recs[0].X - XRef[recs[0].Y]) * propX;
                avy += (recs[0].Y - YRef[recs[0].X]) * propY;
                a += 1;
                if (a < 2) return;
                SetCursorPos(avx/2,avy/2);
                mouse_event(0x0002, 0, 0, 0, 0);
                flagF = true;
                a = 0;
                avx = 0;
                avy = 0;
            }
            else
            {
                if (flagF)
                {
                    a = 0;
                    avx = 0;
                    avy = 0;
                    mouse_event(0x0004, 0, 0, 0, 0);
                    flagF = false;
                }
            }
        }

        private void Form_Pantalla_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myCamera != null)
            {
                if (myCamera.IsRunning)
                {
                    myCamera.Stop();
                }
            }
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();

            }
        }

        private void AbrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void CerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myCamera.Stop();
        }

        private void TrayMinimizerForm_SizeChanged(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.BalloonTipText = "App Minimizada";
                notifyIcon1.ShowBalloonTip(500);
                notifyIcon1.Visible = true;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.BalloonTipText = "App Maximizada";
                notifyIcon1.ShowBalloonTip(500);
                notifyIcon1.Visible = false;
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                String res = serialPort1.ReadLine().Trim();
                if (res.Contains("9C1D58A3C37E"))
                {
                    numDev = res.Split(':')[1].Split('=')[0];
                    serialPort1.Write("AT+CONN" + numDev);
                }
                if (res.Contains("*Borrar*"))
                {
                    SendKeys.SendWait("%s");
                    serialPort1.DiscardOutBuffer();
                }
                if (res.Contains("*Color*"))
                {
                    SendKeys.SendWait("%x");
                    serialPort1.DiscardOutBuffer();
                }
               


            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error: Verifique la conexión con el receptor bluethooth "+System.Environment.NewLine + ex.Message);
            }

        }

        private void TrayMinimizerForm_Load(object sender, EventArgs e)
        {
            
                String[] puertos = SerialPort.GetPortNames();
                try
                {
                    serialPort1.PortName = puertos[0];
                    if (!serialPort1.IsOpen)
                    {
                        serialPort1.Open();
                        serialPort1.Write("AT+INQ");
                       
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se ha podido abrir la comunicación con el receptor Bluetooth ");
                    this.Close();
                }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            while (true)
            {
                if (serialPort1.IsOpen)
                {
                    break;
                }
                else {
                    MessageBox.Show("La conexión con el receptor bluetooth se ha perdido"+System.Environment.NewLine+"Verifique la conexión");
                    String[] puertos = SerialPort.GetPortNames();
                    if (puertos.Length != 0)
                    {
                        serialPort1.PortName = puertos[0];
                        if (!serialPort1.IsOpen)
                        {
                            serialPort1.Open();
                            serialPort1.Write("AT+INQ");

                        }
                    }

                }
            }
        }
    }
}