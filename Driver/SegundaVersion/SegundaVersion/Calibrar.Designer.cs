﻿namespace SegundaVersion
{
    partial class Calibrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calibrar));
            this.blanco4 = new System.Windows.Forms.Panel();
            this.blanco3 = new System.Windows.Forms.Panel();
            this.blanco2 = new System.Windows.Forms.Panel();
            this.blanco1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMostrar = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // blanco4
            // 
            this.blanco4.BackgroundImage = global::SegundaVersion.Properties.Resources.blancoDisparo40x40;
            this.blanco4.Location = new System.Drawing.Point(728, 305);
            this.blanco4.Margin = new System.Windows.Forms.Padding(2);
            this.blanco4.Name = "blanco4";
            this.blanco4.Size = new System.Drawing.Size(27, 26);
            this.blanco4.TabIndex = 14;
            this.blanco4.Visible = false;
            // 
            // blanco3
            // 
            this.blanco3.BackgroundImage = global::SegundaVersion.Properties.Resources.blancoDisparo40x40;
            this.blanco3.Location = new System.Drawing.Point(18, 305);
            this.blanco3.Margin = new System.Windows.Forms.Padding(2);
            this.blanco3.Name = "blanco3";
            this.blanco3.Size = new System.Drawing.Size(27, 26);
            this.blanco3.TabIndex = 13;
            this.blanco3.Visible = false;
            // 
            // blanco2
            // 
            this.blanco2.BackgroundImage = global::SegundaVersion.Properties.Resources.blancoDisparo40x40;
            this.blanco2.Location = new System.Drawing.Point(728, 15);
            this.blanco2.Margin = new System.Windows.Forms.Padding(2);
            this.blanco2.Name = "blanco2";
            this.blanco2.Size = new System.Drawing.Size(27, 26);
            this.blanco2.TabIndex = 12;
            this.blanco2.Visible = false;
            // 
            // blanco1
            // 
            this.blanco1.BackgroundImage = global::SegundaVersion.Properties.Resources.blancoDisparo40x40;
            this.blanco1.Location = new System.Drawing.Point(18, 15);
            this.blanco1.Margin = new System.Windows.Forms.Padding(2);
            this.blanco1.Name = "blanco1";
            this.blanco1.Size = new System.Drawing.Size(27, 26);
            this.blanco1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(14, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "1) Presionar esquina Sup. Izq. de la Pantalla ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(14, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(267, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "2) Presionar esquina Sup. Der. de la Pantalla ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(14, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(257, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "3) Presionar esquina Inf. Izq. de la Pantalla ";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SegundaVersion.Properties.Resources.wallpaper_22;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtMostrar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(238, 129);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 201);
            this.panel1.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(14, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(256, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "4) Presionar esquina Inf. Der. de la Pantalla";
            // 
            // txtMostrar
            // 
            this.txtMostrar.Location = new System.Drawing.Point(88, 162);
            this.txtMostrar.Name = "txtMostrar";
            this.txtMostrar.Size = new System.Drawing.Size(100, 20);
            this.txtMostrar.TabIndex = 4;
            // 
            // Calibrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SegundaVersion.Properties.Resources._1280x720Fondo_Naranja;
            this.ClientSize = new System.Drawing.Size(783, 541);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.blanco4);
            this.Controls.Add(this.blanco3);
            this.Controls.Add(this.blanco2);
            this.Controls.Add(this.blanco1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Calibrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CALIBRACIÓN";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Calibrar_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel blanco4;
        private System.Windows.Forms.Panel blanco3;
        private System.Windows.Forms.Panel blanco2;
        private System.Windows.Forms.Panel blanco1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMostrar;
    }
}